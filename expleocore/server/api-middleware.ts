let fs = require('fs');
var jwt = require('jsonwebtoken');

const AUTH_HEADER = 'auth_token';

module.exports = function (req, res, next) {
    let hasPermission = true;
    let statusCode = 401;
    let errorResponse = {
        description: "Unauthorized request"
    };
    const url = req.url;
    if (url.startsWith('/v1')) { // Regex match if required
        hasPermission = false;
        const authToken = req.headers[AUTH_HEADER];
        if (authToken && typeof authToken === 'string') {
            // verify a token asymmetric
            const cert = fs.readFileSync('rsa_pub_key');  // get public key
            jwt.verify(authToken, cert, function (err, decoded) {
                if (err) {
                    hasPermission = false;
                    errorResponse['message'] = err.message;
                    console.error("ERR: ", JSON.stringify(err));
                } else
                    hasPermission = true;
            });
        } else {
            res.statusCode = 401;
            errorResponse['message'] = "Missing or invalid token";
        }
    }
    if (hasPermission) {
        next()
    } else {
        res.statusCode = statusCode;
        res.send(errorResponse);
    }
}