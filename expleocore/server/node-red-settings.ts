let { v4: uuidv4 } = require('uuid');
let apiMiddleware = require('./api-middleware.ts');
let path = require("path");

// Returns the settings object - see default settings.js file for other options
module.exports = {
    httpAdminRoot: "/exp-red",
    httpNodeMiddleware: process.env.ENABLE_API_MIDDLEWARE ? apiMiddleware : undefined,
    httpNodeRoot: "/api",
    userDir: "/home/flows/",
    flowFile: `flows/${process.env.FLOW_FILE || 'exp-flows.json'}`,
    functionGlobalContext: { // enables global context
        "uuidv4": uuidv4,
        "env": process.env,
        "ObjectId": process.env.DB_TYPE == 'MONGODB' ? require('mongodb').ObjectID : undefined // required only for MongoDB apps
    },
    adminAuth: {
        type: "credentials",
        users: [{
            username: "admin",
            password: "$2a$08$APB7h2lI/lWev2173b3GcO5CG/an2AMOpsdxGq16AM78GBr.zanwi", // Test@123
            permissions: "*"
        }]
    },
    disableEditor: process.env.ENV !== 'LOCAL',
    logging: {
        // Console logging
        console: {
            level: process.env.LOGGING_LEVEL || 'INFO',
            metrics: false,
            audit: false
        }
    },
    editorTheme: {
        page: {
            title: "Expleo-RED",
            favicon: path.join(__dirname, "assets/images/favicon.png"),
            css: path.join(__dirname, "assets/css/styles.css"),
            // scripts: ["/absolute/path/to/custom/script/file", "/another/script/file"]
        },
        header: {
            title: " ",
            image: path.join(__dirname, "assets/images/logo.png"), // or null to remove image
            // url: "http://nodered.org" // optional url to make the header text/image a link to this url
        },
        deployButton: {
            type: "simple",
            label: `Save & Run <i class="fa fa-play" aria-hidden="true"></i>`,
            icon: null // or null to remove image
        },
        // userMenu: false, // Hide the user-menu even if adminAuth is enabled
        login: {
            title: "Login",
            image: path.join(__dirname, "assets/images/banner.png") // a 256x256 image
        },
        logout: {
            redirect: process.env.NODE_RED_LOGOUT_REDIRECT_URL || "http://nodered.org"
        },
        projects: {
            enabled: false // Enable the projects feature
        }
    }
};
