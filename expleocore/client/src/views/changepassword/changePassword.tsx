import React from "react";
import "antd/dist/antd.css";
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import PropTypes from "prop-types";
import {
  Form,
  Input,
  Button,
  Switch,
  Select,
  Card,
  Breadcrumb,
  Row,
  Alert,
  Col
} from "antd";

import { changePassword } from "../../redux/actions/changePassword/changePasswordAction";
import FormItem from "antd/lib/form/FormItem";
import i18n, { lang, setInitialLocale } from '../../i18n'

const { TextArea } = Input;
const { Option } = Select;

type MyProps = {
    loginData:any,
    form: any,
    history: any,
    changePassword:any,
    email:any
  };

type MyState = {
    confirmDirty: boolean,
    successMessage: string,
    errorMessage: string
  };
class Changepassword extends React.Component<MyProps,MyState> {    
  
  constructor(props:any){
      super(props)
      this.state = {
        confirmDirty: false,
        successMessage: '',
        errorMessage:''
      }
  }  

  handleSubmit = (e:any) => {
    e.preventDefault();
    console.log("In handle Submit");
    this.props.form.validateFieldsAndScroll((err, values) => {
        console.log("In validate form",values)
      if (!err) {
        this.props.changePassword(values);
        let successMessage= i18n.t(lang.sucess_messages["password_update"]);
        this.setState({successMessage});
        setTimeout(() => {
          this.props.history.push(`/home`);
        }, 1000)     
        //alert("Password Successfully updated..");
        //this.props.history.push("/home");
      } else {
        let errorMessage= i18n.t(lang.error_messages["password_mismatch"]);
        this.setState({errorMessage});
       // alert("Enter Password correctly..");
      }
    });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue("NewPassword")) {
      callback("Two passwords that you enter is inconsistent!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(["ConfirmPassword"], { force: true });
    }
    callback();
  };

  setEmail = callback => {
    const { form } = this.props.email;

    callback();
  };

handleClose() {
    return <Redirect to='/home'/>
  }

  redirecthome = (e:any) => {
    e.preventDefault();
    return <Redirect to='/home'/>
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { form } = this.props;

    return (
      <Form onSubmit={this.handleSubmit}>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    {i18n.t(lang.breadcrumbs["home"])}{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{i18n.t(lang.breadcrumbs["change_password"])}</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row>
            <div className="float-left">
              <h5>Change Password</h5>
            </div>
            <div className="float-right">
              <Button type="primary" htmlType="submit">
              {i18n.t(lang.buttons["update_password"])}
              </Button>
            </div>
            <br></br>
            <hr className="hrline"></hr>
          </Row>

          {/* <Row type="flex" justify="center">
            <Col span={8}>
              {Global.alertContent.message ? (
                <Alert
                  message={Global.alertContent.status.toUpperCase()}
                  description={Global.alertContent.message}
                  type={Global.alertContent.status}
                  closable={Global.alertContent.closable}
                  closeText="OK"
                  afterClose={this.handleClose}
                  showIcon
                />
              ) : null}
            </Col>
          </Row> */}
          <Row>
            <div className="cardheader">
              <Card className="antcardborder">
                <div className="row">
                  <div className="col-md-12 col-sm-12">
                    <div className="successmsg">{this.state.successMessage}</div>
                    <div className="errormsg">{this.state.errorMessage}</div>
                  </div>
                  <div className="col-md-6 col-sm-12">
                    <Form.Item label={i18n.t(lang.labels["old_password"])}>
                      {getFieldDecorator("OldPassword", {
                        rules: [
                          {
                            message: "Please enter OldPassword",
                            required: true
                          },
                          {
                            max: 100,
                            message:
                              "OldPassword should be accept maximum 100 characters"
                          },
                          {
                            message:
                              "OldPassword should be minimum 2 characters",
                            min: 2
                          }
                        ]
                      })(<Input.Password />)}
                    </Form.Item>
                  </div>

                  <div className="col-md-6 col-sm-12">
                    <Form.Item label={i18n.t(lang.labels["new_password"])} hasFeedback>
                      {getFieldDecorator("NewPassword", {
                        rules: [
                          {
                            message: "Please enter NewPassword",
                            required: true
                          },
                          {
                            max: 100,
                            message:
                              "NewPassword should be accept maximum 100 characters"
                          },
                          {
                            message:
                              "NewPassword should be minimum 2 characters",
                            min: 2
                          },
                          {
                            validator: this.validateToNextPassword
                          }
                        ]
                      })(<Input.Password />)}
                    </Form.Item>
                  </div>

                  <div className="col-md-6 col-sm-12">
                    <Form.Item label={i18n.t(lang.labels["confirm_password"])} hasFeedback>
                      {getFieldDecorator("ConfirmPassword", {
                        rules: [
                          {
                            message: "Please enter ConfirmPassword",
                            required: true
                          },
                          {
                            max: 100,
                            message:
                              "ConfirmPassword should be accept maximum 100 characters"
                          },
                          {
                            message:
                              "ConfirmPassword should be minimum 2 characters",
                            min: 2
                          },
                          {
                            validator: this.compareToFirstPassword
                          }
                        ]
                      })(<Input.Password />)}
                    </Form.Item>
                  </div>

                  <div>
                    <Form.Item>
                      {getFieldDecorator("Email", {
                        initialValue: this.props.email
                      })(<Input hidden />)}
                    </Form.Item>
                  </div>
                </div>

                {/* <Form.Item>
                  <Button type="primary" htmlType="submit">
                    Submit
                  </Button>
                </Form.Item> */}
              </Card>
            </div>
          </Row>
        </div>
      </Form>
    );
  }
}

// Changepassword.propTypes = {
//   changePassword: PropTypes.func.isRequired
// };

const mapStateToProps = state => ({
  email: state.loginData.username,
  loginData:state.loginData,
});

const Createpasswordform = Form.create({ name: "register" })(Changepassword);

export default connect(
  mapStateToProps,
  { changePassword }
)(Createpasswordform);
