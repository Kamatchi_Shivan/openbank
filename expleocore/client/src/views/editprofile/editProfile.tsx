import React from "react";
import "antd/dist/antd.css";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateUser } from "../../redux/actions/profileUpdate/profileUpdateAction"
import ComponentButtons from "../common/ComponentButton";
//import { emptySelectedProject } from "../redux/actions/loginAction";
import {
  Form,
  Input,
  Button,
  Switch,
  Select,
  Card,
  Breadcrumb,
  Row
} from "antd";
import { Redirect } from 'react-router-dom'
import i18n, { lang, setInitialLocale } from '../../i18n'
//import CommonButtons from "./commonButton";

const { Option } = Select;
type MyProps = {
    loginData:any,
    form: any,
    history: any,
    updateUser:any
  };

  type MyState = {
    disabled: boolean,
    modifyprofile: string,
    successMessage: string,
    errorMessage: string
  };

class EditProfile extends React.Component<MyProps,MyState> {
  constructor(props:any) {
    super(props);
    this.state = { 
      disabled: true, 
      modifyprofile: "View Profile",
      successMessage: '',
      errorMessage:''
    };
  }
  componentWillMount() {
    console.log("loginData:>>>>>>>", this.props.loginData);
    console.log("UserName:>>>>>>>", this.props.loginData.username);
  }
  handleEdit = (e:any) => {
    this.setState({ disabled: !this.state.disabled });
    this.setState({ modifyprofile: "Edit Profile" });
  };

  handleCancel = (e:any) => {
    this.setState({ disabled: true });
    this.setState({ modifyprofile: "View Profile" });
  };
 
  renderRedirect = () =>{
    if (this.props.loginData.redirect) {
        return <Redirect to='/home'/>
    }
  }
//   buttonClicked(){
//       this.renderRedirect();
//   }

redirecthome = (e:any) => {
    e.preventDefault();
    this.props.history.push("/home");
  };

  handleSubmit = (e:any) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err:any, values:any) => {
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.updateUser(this.props.loginData.username,this.props.loginData.status,values);
        let successMessage = i18n.t(lang.sucess_messages["profile_update"]);
        this.setState({successMessage});
        setTimeout(() => {
          this.props.history.push(`/home`);
        }, 1000)     
        //alert("Successfully updated..");
        //this.props.history.push("/home");
      } else {
        let errorMessage= i18n.t(lang.error_messages["enter_correct_details"]);
        this.setState({errorMessage});
        //alert("Enter correct details..");
      }
    });
  };  
  
  render() {
    console.log("LoginData>@@@@@@@@@@",this.props.loginData)
    const { getFieldDecorator } = this.props.form;
    return (
      <Form>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
            <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    {i18n.t(lang.breadcrumbs["home"])}{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{this.state.modifyprofile}</Breadcrumb.Item>
              </Breadcrumb>
            </Row>
          </div>

          <Row>
            <div className="float-left">
              <h5>{this.state.modifyprofile}</h5>
            </div>
            <ComponentButtons
              handleEdit={this.handleEdit}
              handleUpdate={this.handleSubmit}
              handleCancel={this.handleCancel}
            ></ComponentButtons>
            <br></br>
            <hr className="hrline"></hr>
          </Row>

          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                  <div className="col-md-12 col-sm-12">
                    <div className="successmsg">{this.state.successMessage}</div>
                    <div className="errormsg">{this.state.errorMessage}</div>
                  </div>
                <div className="col-md-6 col-sm-12">
                  <Form.Item label={i18n.t(lang.labels["first_name"])}>
                    {getFieldDecorator("firstname", {
                      initialValue: this.props.loginData.firstname,
                      rules: [
                        {
                          message: "Please enter FirstName",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "FirstName should be accept maximum 100 characters"
                        },
                        {
                          message: "FirstName should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                    )}
                  </Form.Item>
                </div>
                <div className="col-md-6 col-sm-12">
                  <Form.Item label={i18n.t(lang.labels["middle_name"])}>
                    {getFieldDecorator("middlename", {
                      initialValue: this.props.loginData.middlename,
                      rules: [
                        {
                          message: "Please enter Middle Name",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "Middle Name should be accept maximum 100 characters"
                        },
                        {
                          message: "Middle Name should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                    )}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item label={i18n.t(lang.labels["last_name"])}>
                    {getFieldDecorator("lastname", {
                      initialValue: this.props.loginData.lastname,
                      rules: [
                        {
                          message: "Please enter LastName",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "LastName should be accept maximum 100 characters"
                        },
                        {
                          message: "LastName should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                    )}
                  </Form.Item>
                </div>

                {/* <div className="col-md-6 col-sm-12">
                  <Form.Item label="User Name">
                    {getFieldDecorator("username", {
                      initialValue: this.props.loginData.username,
                      rules: [
                        {
                          message: "Please enter UserName",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "UserName should be accept maximum 100 characters"
                        },
                        {
                          message: "UserName should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div> */}
              </div>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}

const mapStateToProps = (state:any) => ({
  //element: state.user.editProfile,
  loginData:state.loginData,
//   uid: state.loginData.id,
//   uname: state.loginData.userName
});
 const EditProfileForm = Form.create({ name: "register" })(EditProfile);

export default connect(
   mapStateToProps,
  { updateUser }
 )
 (EditProfileForm);
