import React, { Component } from 'react'

export default class GoogleCaptcha extends Component {
    render() {
        return (
            <form name="myForm">
                <div className="form-control captcha-box">
                    <div className="g-recaptcha" data-sitekey="6LcyXugUAAAAAGB45qHzczELbZvoQ-CNKz06fFXz"></div>
                </div>
            </form>
        )
    }
}
