import React from "react";
import Button from '@material-ui/core/Button';
//import { Button } from "antd";
import i18n, { lang, setInitialLocale } from '../../i18n'

type MyProps = {
    handleEdit: any,
    handleCancel:any,
    handleUpdate:any
  };
type MyState = {
    isReadOnly: boolean
  };
class ComponentButtons extends React.Component<MyProps,MyState> {
  constructor(props:any) {
    super(props);
    this.state = { isReadOnly: true };
  }

  handleEdit = (e:any) => {
    e.preventDefault();
    this.setState({ isReadOnly: false });
    this.props.handleEdit();
  };

  handleCancel = (e:any) => {
    e.preventDefault();
    this.setState({ isReadOnly: true });
    this.props.handleCancel();
  };

  render() {
    return (
      <div className="float-right">
        {this.state.isReadOnly ? (
          <div className="float-right">
            <Button 
            size="small"
            variant="contained" 
            color="primary"
            onClick={this.handleEdit}>
            {i18n.t(lang.buttons["edit"])}
            </Button>&nbsp;&nbsp;&nbsp;&nbsp;
            
            {/* <Button type="primary" htmlType="submit" onClick={this.handleEdit}>
            {i18n.t(lang.buttons["edit"])}
            </Button>&nbsp;&nbsp;&nbsp;&nbsp; */}
            {/* <Button
              onClick={this.props.handleDelete}
              className="btn btn-danger"
              style={{ float: "right" }}
            >
              Delete
            </Button> */}
          </div>
        ) : (
          <div className="float-left">
             <Button 
                size="small"
                variant="contained" 
                color="primary"
                onClick={this.props.handleUpdate}>
                {i18n.t(lang.buttons["update"])}
              </Button>&nbsp;&nbsp;&nbsp;&nbsp;
              <Button
                variant="contained" 
                size="small"
                style={{ float: "right" }}
                onClick={this.handleCancel}
                //className="btn btn-warning"
              >
              {i18n.t(lang.buttons["cancel"])}
              </Button>
            {/* <Button
              type="primary"
              htmlType="submit"
              onClick={this.props.handleUpdate}
            >
              {i18n.t(lang.buttons["update"])}
            </Button>&nbsp;&nbsp;&nbsp;&nbsp;
            <Button
              style={{ float: "right" }}
              onClick={this.handleCancel}
              className="btn btn-warning"
            >
              {i18n.t(lang.buttons["cancel"])}
            </Button> */}
          </div>
        )}
      </div>
    );
  }
}

export default ComponentButtons;
