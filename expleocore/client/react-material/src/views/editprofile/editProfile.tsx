import React from "react";
import "antd/dist/antd.css";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { updateUser } from "../../redux/actions/profileUpdate/profileUpdateAction"
import ComponentButtons from "../common/ComponentButton";
//import { emptySelectedProject } from "../redux/actions/loginAction";
import {
  Form
} from "antd";
import { Redirect } from 'react-router-dom'
import i18n, { lang, setInitialLocale } from '../../i18n'
import { Theme, withStyles, FormControl, InputLabel, Input, InputAdornment } from '@material-ui/core';
//import CommonButtons from "./commonButton";
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import Card from '@material-ui/core/Card';
import TableRow from '@material-ui/core/TableRow';
//import Button from '@material-ui/core/Button';

//const { Option } = Select;
// type MyProps = {
//     loginData:any,
//     form: any,
//     history: any,
//     updateUser:any
//   };

//   type MyState = {
//     disabled: boolean,
//     modifyprofile: string,
//     successMessage: string,
//     errorMessage: string
//   };

  interface MyProps {
    loginData?:any,
    form?: any,
    history?: any,
    updateUser?:any
  }
  
  interface MyState {
    firstName:string;
    middleName:string;
    lastName:string;
    disabled: boolean;
    modifyprofile: string;
    successMessage: string;
    errorMessage: string;
  }

class EditProfile extends React.Component<MyProps,MyState> {

  componentWillMount() {
    console.log("loginData:>>>>>>>", this.props.loginData);
    console.log("UserName:>>>>>>>", this.props.loginData.firstname);
  }

  public state = {
      firstName: this.props.loginData.firstname,
      middleName:this.props.loginData.middlename,
      lastName:this.props.loginData.lastname,
      disabled: true, 
      modifyprofile: "View Profile",
      successMessage: "",
      errorMessage:""
 };

 private handleFirstnameChange = (event: any) => {
  console.log("Email Target VAlue",event)
  this.setState({ firstName: event.target.value })
}

private handleMiddlenameChange = (event: any) => {
  this.setState({ middleName: event.target.value })
}

private handleLastnameChange = (event: any) => {
  this.setState({ lastName: event.target.value })
}
  // constructor(props:any) {
  //   super(props);
  //   this.state = { 
  //     disabled: true, 
  //     modifyprofile: "View Profile",
  //     successMessage: '',
  //     errorMessage:''
  //   };
  // }
  
  handleEdit = (e:any) => {
    this.setState({ disabled: !this.state.disabled });
    this.setState({ modifyprofile: "Edit Profile" });
  };

  handleCancel = (e:any) => {
    this.setState({ disabled: true });
    this.setState({ modifyprofile: "View Profile" });
  };
 
  renderRedirect = () =>{
    if (this.props.loginData.redirect) {
        return <Redirect to='/home'/>
    }
  }
//   buttonClicked(){
//       this.renderRedirect();
//   }

redirecthome = (e:any) => {
    e.preventDefault();
    this.props.history.push("/home");
  };

  // handleSubmit = (e:any) => {
  //   e.preventDefault();
  //   this.props.form.validateFieldsAndScroll((err:any, values:any) => {
  //     if (!err) {
  //       console.log("Received values of form: ", values);
  //       this.props.updateUser(this.props.loginData.username,this.props.loginData.status,values);
  //       let successMessage = i18n.t(lang.sucess_messages["profile_update"]);
  //       this.setState({successMessage});
  //       setTimeout(() => {
  //         this.props.history.push(`/home`);
  //       }, 1000)     
  //       //alert("Successfully updated..");
  //       //this.props.history.push("/home");
  //     } else {
  //       let errorMessage= i18n.t(lang.error_messages["enter_correct_details"]);
  //       this.setState({errorMessage});
  //       //alert("Enter correct details..");
  //     }
  //   });
  // };  
  private handleSubmit = () => {
    console.log("State",this.state)
    this.props.form.validateFieldsAndScroll((err:any, values:any) => {
      values = this.state;
      if (!err) {
        console.log("Received values of form: ", values);
        this.props.updateUser(this.props.loginData.username,this.props.loginData.status,values);
        let successMessage = i18n.t(lang.sucess_messages["profile_update"]);
        this.setState({successMessage});
        setTimeout(() => {
        this.props.history.push(`/home`);
        }, 1000)     
        //alert("Successfully updated..");
        //this.props.history.push("/home");
        }
        else {
            let errorMessage= i18n.t(lang.error_messages["enter_correct_details"]);
            this.setState({errorMessage});
            //alert("Enter correct details..");
        }     
  });
}
  public render(): JSX.Element {
    console.log("LoginData>@@@@@@@@@@",this.props.loginData)
    //const { getFieldDecorator } = this.props.form;
    return (
      <Form>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
          <TableRow>
            <Breadcrumbs aria-label="breadcrumb">
              <Link color="inherit" href="/" onClick={this.redirecthome}>
              {i18n.t(lang.breadcrumbs["home"])}
              </Link>
               <Typography color="textPrimary">{this.state.modifyprofile}</Typography>
            </Breadcrumbs>
              {/* <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    {i18n.t(lang.breadcrumbs["home"])}{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{this.state.modifyprofile}</Breadcrumb.Item>
              </Breadcrumb> */}
            </TableRow>
          </div>

         
            <br/>
            <div className="float-left">
              <h5>{this.state.modifyprofile}</h5>
            </div>
            <ComponentButtons
              handleEdit={this.handleEdit}
              handleUpdate={this.handleSubmit}
              handleCancel={this.handleCancel}
            ></ComponentButtons>
            <br></br>
            <hr className="hrline"></hr>
          

          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                  <div className="col-md-12 col-sm-12">
                    <div className="successmsg">{this.state.successMessage}</div>
                    <div className="errormsg">{this.state.errorMessage}</div>
                  </div>
                <div className="col-md-6 col-sm-12">
                <br />
                <TextField
                    disabled={this.state.disabled? true:false}
                    fullWidth={true}
                    id="firstName"
                    label={i18n.t(lang.labels["first_name"])}
                    required= {true}
                    //defaultValue= {this.props.loginData.firstname}
                    value = {this.state.firstName}
                    onChange ={this.handleFirstnameChange}
                 />
                </div>
                <div className="col-md-6 col-sm-12">
                <br />
                <TextField
                    disabled={this.state.disabled? true:false}
                    fullWidth={true}
                    id="middleName"
                    label={i18n.t(lang.labels["middle_name"])}
                    required= {true}
                    //defaultValue= {this.props.loginData.middlename}
                    value = {this.state.middleName}
                    onChange ={this.handleMiddlenameChange}
                 />
                </div>
                <div className="col-md-6 col-sm-12">
                <br />
                <TextField
                    disabled={this.state.disabled? true:false}
                    fullWidth={true}
                    id="lastName"
                    label={i18n.t(lang.labels["last_name"])}
                    required= {true}
                    //defaultValue= {this.props.loginData.lastname}
                    value = {this.state.lastName}
                    onChange ={this.handleLastnameChange}
                 />
                </div>
                <div className="col-md-12 col-sm-24">
                <br />
                </div>

                {/* <FormControl required={true} fullWidth={true}>
                        <InputLabel htmlFor="firstName">First Name</InputLabel>
                        <Input
                            defaultValue={this.props.loginData.middlename}
                            value={this.state.firstName}
                            onChange={this.handleFirstnameChange}
                            id="firstName"
                        />
                    </FormControl> */}
                
                {/* <div className="col-md-6 col-sm-12">
                  <Form.Item label="User Name">
                    {getFieldDecorator("username", {
                      initialValue: this.props.loginData.username,
                      rules: [
                        {
                          message: "Please enter UserName",
                          required: true
                        },
                        {
                          max: 100,
                          message:
                            "UserName should be accept maximum 100 characters"
                        },
                        {
                          message: "UserName should be minimum 2 characters",
                          min: 2
                        }
                      ]
                    })(
                      <Input disabled={this.state.disabled ? "disabled" : ""} />
                    )}
                  </Form.Item>
                </div> */}
              </div>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}

const mapStateToProps = (state:any) => ({
  //element: state.user.editProfile,
  loginData:state.loginData,
//   uid: state.loginData.id,
//   uname: state.loginData.userName
});
 const EditProfileForm = Form.create({ name: "register" })(EditProfile);

export default connect(
   mapStateToProps,
  { updateUser }
 )
 (EditProfileForm);
