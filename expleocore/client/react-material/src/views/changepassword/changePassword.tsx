import React from "react";
import "antd/dist/antd.css";
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import PropTypes from "prop-types";
import {
  Form
} from "antd";
import { changePassword } from "../../redux/actions/changePassword/changePasswordAction";
import FormItem from "antd/lib/form/FormItem";
import i18n, { lang, setInitialLocale } from '../../i18n'
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Link from '@material-ui/core/Link';
import Card from '@material-ui/core/Card';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';

// const { TextArea } = Input;
// const { Option } = Select;

// type MyProps = {
//     loginData:any,
//     form: any,
//     history: any,
//     changePassword:any,
//     email:any
//   };

// type MyState = {
//     confirmDirty: boolean,
//     successMessage: string,
//     errorMessage: string
//   };

  interface MyProps {
    loginData?:any,
    form?: any,
    history?: any,
    changePassword?:any,
    email?:any
  }
  
  interface MyState {
    confirmDirty?: boolean;
    successMessage?: string;
    errorMessage?: string;
    oldPassword?: string;
    newPassword?:string;
    confirmPassword?:string;
    email?:string;
    passwordError?:any;
  }

class Changepassword extends React.Component<MyProps,MyState> {  
  
  componentWillMount() {
    console.log("loginData:>>>>>>>", this.props.loginData);
    console.log("UserName:>>>>>>>", this.props.loginData.email);
  }
  
  public state = {
    email:this.props.email,
    oldPassword: "",
    newPassword: "",
    confirmPassword: "",
    confirmDirty: false,
    successMessage: "",
    errorMessage:"",
    passwordError:""
  };

  // constructor(props:any){
  //     super(props)
  //     this.state = {
  //       confirmDirty: false,
  //       successMessage: '',
  //       errorMessage:''
  //     }
  // }  

  validate = () => {
    let passwordError = "";

    if (this.state.newPassword!= this.state.confirmPassword) {
      passwordError = "Password Mismatch"; 
    }

    if (passwordError) {
      this.setState({ passwordError });
      return false;
    }

    return true;
  };

  private handleSubmit = () => {
    console.log("State",this.state)
    const isValid = this.validate();
    if(isValid){
    this.props.form.validateFieldsAndScroll((err:any, values:any) => {
      values = this.state;
      if (!err) {
        this.props.changePassword(values);
        let successMessage= i18n.t(lang.sucess_messages["password_update"]);
        this.setState({successMessage});
        setTimeout(() => {
          this.props.history.push(`/home`);
        }, 1000)     
        //alert("Password Successfully updated..");
        //this.props.history.push("/home");
      } else {
        let errorMessage= i18n.t(lang.error_messages["password_mismatch"]);
        this.setState({errorMessage});
       // alert("Enter Password correctly..");
      }
    });
  }
  // else{
  //   let errorMessage= i18n.t(lang.error_messages["password_mismatch"]);
  //   this.setState({errorMessage});
  // }
  };

  // handleSubmit = (e:any) => {
  //   e.preventDefault();
  //   console.log("In handle Submit");
  //   this.props.form.validateFieldsAndScroll((err, values) => {
  //       console.log("In validate form",values)
  //     if (!err) {
  //       this.props.changePassword(values);
  //       let successMessage= i18n.t(lang.sucess_messages["password_update"]);
  //       this.setState({successMessage});
  //       setTimeout(() => {
  //         this.props.history.push(`/home`);
  //       }, 1000)     
  //       //alert("Password Successfully updated..");
  //       //this.props.history.push("/home");
  //     } else {
  //       let errorMessage= i18n.t(lang.error_messages["password_mismatch"]);
  //       this.setState({errorMessage});
  //      // alert("Enter Password correctly..");
  //     }
  //   });
  // };

  
  private handleOldPasswordChange = (event: any) => {
    console.log("OldPassword",event.target.value)
    this.setState({ oldPassword: event.target.value })
  }
  private handleNewPasswordChange = (event: any) => {
    console.log("NewPassword",event.target.value)
    this.setState({ newPassword: event.target.value })
  }
  private handleConfirmPasswordChange = (event: any) => {
    console.log("ConfirmPassword",event.target.value)
    console.log("ConfirmPassword state",this.state)
    this.setState({ confirmPassword: event.target.value })
  }

  
  compareToFirstPassword = (rule, value, callback) => {
    const { form } = this.props;
    if (value && value !== form.getFieldValue("NewPassword")) {
      callback("Two passwords that you enter is inconsistent!");
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    console.log("In validate New password",value, this.props)
    const { form } = this.props;
    if (value && this.state.confirmDirty) {
      form.validateFields(["ConfirmPassword"], { force: true });
    }
    callback();
  };

  setEmail = callback => {
    const { form } = this.props.email;

    callback();
  };

handleClose() {
    return <Redirect to='/home'/>
  }

  redirecthome = (e:any) => {
    e.preventDefault();
    this.props.history.push("/home");
    //return <Redirect to='/home'/>
  };

  public render(): JSX.Element {
    const { getFieldDecorator } = this.props.form;
    const { form } = this.props;

    return (
      <Form>
        <div className="container-fluid">
          <div className="Breadcrumb-top">
          <TableRow>
            <Breadcrumbs aria-label="breadcrumb">
              <Link color="inherit" href="/" onClick={this.redirecthome}>
              {i18n.t(lang.breadcrumbs["home"])}
              </Link>
               <Typography color="textPrimary">{i18n.t(lang.breadcrumbs["change_password"])}</Typography>
            </Breadcrumbs>
            </TableRow>
            {/* <Row>
              <Breadcrumb>
                <Breadcrumb.Item>
                  {" "}
                  <Button type="link" onClick={this.redirecthome}>
                    {" "}
                    {i18n.t(lang.breadcrumbs["home"])}{" "}
                  </Button>
                </Breadcrumb.Item>
                <Breadcrumb.Item>{i18n.t(lang.breadcrumbs["change_password"])}</Breadcrumb.Item>
              </Breadcrumb>
            </Row> */}
          </div>

            <br/>
            <div className="float-left">
              <h5>Change Password</h5>
            </div>
            <div className="float-right">
              <Button 
              size="small"
              onClick={this.handleSubmit}
              variant="contained" 
              color="primary">
              {i18n.t(lang.buttons["update_password"])}
              </Button>
            </div>
            <br></br>
            <hr className="hrline"></hr>
            

          {/* <Row type="flex" justify="center">
            <Col span={8}>
              {Global.alertContent.message ? (
                <Alert
                  message={Global.alertContent.status.toUpperCase()}
                  description={Global.alertContent.message}
                  type={Global.alertContent.status}
                  closable={Global.alertContent.closable}
                  closeText="OK"
                  afterClose={this.handleClose}
                  showIcon
                />
              ) : null}
            </Col>
          </Row> */}
          
            <div className="cardheader">
              <Card className="antcardborder">
                <div className="row">
                  <div className="col-md-12 col-sm-12">
                    <div className="successmsg">{this.state.successMessage}</div>
                    <div className="errormsg">{this.state.errorMessage}</div>
                  </div>
                  <div className="col-md-6 col-sm-12">
                  <br />
                     <TextField
                         //disabled={this.state.disabled? true:false}
                         fullWidth={true}
                         id="oldPassword"
                         label={i18n.t(lang.labels["old_password"])}
                         required= {true}
                         type="password"
                         //defaultValue= {this.props.loginData.firstname}
                         value = {this.state.oldPassword}
                         onChange ={this.handleOldPasswordChange}
                      />
                  </div>

                  <div className="col-md-6 col-sm-12">
                  <br />
                     <TextField
                         type="password"
                         //disabled={this.state.disabled? true:false}
                         fullWidth={true}
                         id="newPassword"
                         label={i18n.t(lang.labels["new_password"])}
                         required= {true}
                         //defaultValue= {this.props.loginData.firstname}
                         value = {this.state.newPassword}
                         onChange ={this.handleNewPasswordChange}
                         //validators={this.validateToNextPassword}
                      />
                  </div>

                  <div className="col-md-6 col-sm-12">
                  <br />
                     <TextField
                         type="password"
                         //disabled={this.state.disabled? true:false}
                         fullWidth={true}
                         id="confirmPassword"
                         label={i18n.t(lang.labels["confirm_password"])} 
                         required= {true}
                         //defaultValue= {this.props.loginData.firstname}
                         value = {this.state.confirmPassword}
                         onChange ={this.handleConfirmPasswordChange}
                         //validators={this.compareToFirstPassword}
                      />
                      <div style={{ fontSize: 12, color: "red" }}>
                         {this.state.passwordError}
                      </div>
                  </div>
                  <div className="col-md-12 col-sm-24">
                  <br />
                  </div>

                  {/* <div>
                    <Form.Item>
                      {getFieldDecorator("Email", {
                        initialValue: this.props.email
                      })(<Input hidden />)}
                    </Form.Item>
                  </div> */}
                </div>

                {/* <Form.Item>
                  <Button type="primary" htmlType="submit">
                    Submit
                  </Button>
                </Form.Item> */}
              </Card>
            </div>
        </div>
      </Form>
    );
  }
}

// Changepassword.propTypes = {
//   changePassword: PropTypes.func.isRequired
// };

const mapStateToProps = state => ({
  email: state.loginData.username,
  loginData:state.loginData,
});

const Createpasswordform = Form.create({ name: "register" })(Changepassword);

export default connect(
  mapStateToProps,
  { changePassword }
)(Createpasswordform);
