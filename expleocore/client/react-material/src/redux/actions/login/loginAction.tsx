import { LOGIN, LOGOUT,CHANGE_PASSWORD } from '../types';
import fetch from '../../../utils/leoFetch'
import axios from '../../../utils/leoAxios'
import history from '../../../history';
// import Global from '../../../../src/components/layout/Global/Global.js';
import Global from '../../../components/layout/Global/Global';
import message from '../../../components/messages/message.json'
var jwtDecode = require('jwt-decode');
import {config} from '../../../config/configs'

export const logoutAction = () => (dispatch:any) => {
    let loginData = {
        isLoggedIn: false,
        redirect: false,
        token: null
    };
    localStorage.clear();
    console.log("Logout Lacal storage", localStorage)
    dispatch({
        payload: loginData,
        type: LOGOUT
    })

}

export const loginAction = (values:any) => (dispatch:any) => {
    console.log("loginAction...........", values);

    let payload = {
      userName: values.userName,
      password: values.password
    }

    console.log("loginAction payload...........", payload);

    let loginData = {
        Name: null,
        userName: null,
        isLoggedIn: false,
        password: null, // for reference
        redirect: false,
        token: null
    };

    axios.post(config.authenticationURL, payload,  {
      headers: {
          'Content-Type': 'application/json',
      }
    })
    .then(res => {
            console.log(101, res);
            var token = res.data.token;
            if (token !== null && token !== undefined) {
              var decoded = jwtDecode(token);
              console.log("Decode token",decoded);
              console.log("Decode Username",decoded.username);
                console.log(100000000, res, res.data);
                dispatch({
                    payload: {
                        firstname:decoded.firstname,
                        middlename:decoded.middlename,
                        lastname:decoded.lastname,
                        username:decoded.username,
                        status:decoded.status,
                        userDetail:decoded,
                        isLoggedIn: true,
                        redirect: true,
                        ...res.data
                    },
                    type: LOGIN
                });
            } else {
                //  alert("Username and Password does not match");
                // localStorage.removeItem("token");
                // Global.alertContent = {
                //     closable: true,
                //     message: "Login Failed",
                //     status: "error"
                // }
                dispatch({
                    payload: {
                        ...loginData,
                        message: message.loginErrorMessage
                    },
                    type: LOGIN
                })
            }
        })
}

export const changePassword = (values:any) => async (dispatch:any) => {
    console.log(123, values.Email);
    var options = {
      ConfirmPassword: values.ConfirmPassword,
      Email: values.Email,
      OldPassword: values.OldPassword
    };
    console.log("To backend : ", options);
    await axios.put( "http://localhost:8000/users/changepassword", options).then(resp => {
      if (resp.data.status === "SUCCESS") {
        Global.alertContent = {
          closable: false,
          message: resp.data.userMessage,
          status: "success"
        };
        dispatch({
          payload: {},
          type: CHANGE_PASSWORD
        });
      } else {
        Global.alertContent = {
          closable: true,
          message: resp.data.userMessage,
          status: "error"
        };
        dispatch({
          payload: {},
          type: CHANGE_PASSWORD
        });
      }
    });
  };
