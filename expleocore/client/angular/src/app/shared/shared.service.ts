import { Injectable } from '@angular/core';

const LANG_KEY = "SELECTED_LANGUAGE";

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private language = 'en';

  private user: User;

  constructor() { }

  get selectedLanguage() {
    let lang = this.language;
    if (!lang)
      lang = localStorage.getItem(LANG_KEY);
    return lang;
  }

  set selectedLanguage(lang: string) {
    this.language = lang;
    localStorage.setItem(LANG_KEY, lang);
  }

  get currentUser() {
    return this.user;
  }

  set currentUser(user: User) {
    this.user = user;
  }
}
