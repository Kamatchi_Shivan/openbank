class User {
    emailAddress: String;
    password: String;
    firstName: String;
    lastName: String;
    role: String;
}

class Project {
    name: String;
    type: String;
    client: String;
    description: String;
    createdDate: Date;
    createdBy: String;
    updatedDate: Date;
    updatedBy: String;
    status: String;
    imgUrl: String;
    progress: number;
    nominees: String[];
    projectDetailsFile: Blob;
}