import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedService } from '../../shared/shared.service';
import { FormGroup } from '@angular/forms';
import { AuthService } from '../auth.service';
import { ToastrService } from '../../shared/toastr/toastr.service';
import { Router } from '@angular/router';

@Component({
  selector: 'exp-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  passInputType = "password"

  emailAddress: String;
  password: String;

  @ViewChild('loginFrm', { static: true })
  loginFrm: FormGroup;

  constructor(public sharedService: SharedService, private authService: AuthService,
    private toastrService: ToastrService, private router: Router) { }

  ngOnInit() {
  }

  login() {
    console.log('this.loginFrm.valid : ', this.loginFrm.valid)
    this.authService.login(this.emailAddress, this.password).subscribe(res => {
      if (res) {
        this.sharedService.currentUser = res;
        this.toastrService.show('success', 'Success', 'Login success');
        this.router.navigate(['/pages']);
      } else {
        this.toastrService.show('danger', 'Invalid Credatial', 'Username or password is invalid');
      }
    }, err => {
      console.error('Error logging in : ', err);
      this.toastrService.show('danger', 'Invalid Credatial', 'Username or password is invalid');
    })
  }
}
