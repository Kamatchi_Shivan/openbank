import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../shared/shared.service';

@Component({
  selector: 'exp-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  constructor(public sharedService: SharedService) { }

  ngOnInit() {
  }

  ngAfterContentInit() {
    document.getElementById('nb-global-spinner').style.display = 'none';
  }

}
