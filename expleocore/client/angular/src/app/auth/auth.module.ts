import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { ThemeModule } from '../@theme/theme.module';
import { NbSelectModule, NbLayoutModule, NbIconModule, NbInputModule } from '@nebular/theme';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { AuthComponent } from './auth/auth.component';

@NgModule({
  declarations: [LoginComponent, AuthComponent],
  imports: [
    CommonModule,
    FormsModule,
    FlexLayoutModule,
    AuthRoutingModule,
    NbLayoutModule,
    ThemeModule,
    NbSelectModule,
    SharedModule,
    NbIconModule,
    NbInputModule
  ]
})
export class AuthModule { }
