import { Injectable } from '@angular/core';

import usersJson from '../data/users.json';
import { ToastrService } from '../shared/toastr/toastr.service';
import { environment } from '../../environments/environment';
import { of, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  apiUrl = environment.apiUrl;

  constructor(private tosterService: ToastrService, private http: HttpClient) { }

  login(username, password): Observable<any> {
    console.log("usersJson : ", usersJson);
    if (environment.mock) {
      const users = usersJson as User[];
      let exist = users.find(user => user.emailAddress === username && user.password === password);
      return of(exist);
    } else {
      // call api or adfs
      const user = {
        'username': username,
        'password': password
      }
      return this.http.post(`${this.apiUrl}auth/login`, user);
    }
  }
}
