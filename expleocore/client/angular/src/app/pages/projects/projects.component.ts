import { Component, OnInit } from '@angular/core';
import projectsJson from '../../data/projects.json';

@Component({
  selector: 'exp-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  sortMetaData = [
    { label: "Progress", value: 'progress' },
    { label: "Created Time", value: "created" },
    { label: "Last Active", value: "updated" }
  ];

  sortBy: string = "created";

  projects: Project[];

  constructor() {
    this.projects = projectsJson as unknown as Project[];
  }

  ngOnInit() {
  }

}
