import { Component } from '@angular/core';

import { MENU_ITEMS } from './pages-menu';

@Component({
  selector: 'exp-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <exp-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </exp-one-column-layout>
  `,
})
export class PagesComponent {

  menu = MENU_ITEMS;
}
