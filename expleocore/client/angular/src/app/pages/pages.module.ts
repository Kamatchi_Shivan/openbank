import { NgModule } from '@angular/core';
import { NbMenuModule, NbSelectModule, NbCardModule, NbInputModule, NbPopoverModule, NbListModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { PagesRoutingModule } from './pages-routing.module';
import { ProjectsComponent } from '../pages/projects/projects.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    FormsModule,
    FlexLayoutModule,
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    NbInputModule,
    NbSelectModule,
    NbCardModule,
    NbPopoverModule,
    NbListModule
  ],
  declarations: [
    PagesComponent,
    ProjectsComponent,
  ],
})
export class PagesModule {
}
