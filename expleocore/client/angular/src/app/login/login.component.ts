import { Component, OnInit } from '@angular/core';
import { FormControl } from "@angular/forms";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
export interface Food {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  /*erson: string = "Arunold";
  selectedValue*/
  title = 'Login'; 

  isLogin: boolean = true;

  selectedValue: string;
  foods: Food[] = [
    { value: 'steak', viewValue: 'Steak' },
    { value: 'pizza', viewValue: 'Pizza' },
    { value: 'tacos', viewValue: 'Tacos' }
  ];

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
  }





  login() {

    console.log('loggin in....', this.username, this.password);
    this.http.post('http://localhost:8001/api/auth/login', { username: this.username, password: this.password }).subscribe(res => {
      console.log(res);
      this.router.navigate(['/home']);
      alert('Login success!!!');
    }, err => {
      console.log(JSON.stringify(err));
      alert(err && err.error && err.error.error || 'Internal Error');
    })
  }


  switchLogin() {
    this.isLogin = !this.isLogin;
    this.username = undefined;
    this.password = undefined;
  }

  signup() {
    this.http.post('http://localhost:8001/api/auth/register', { username: this.username, password: this.password }).subscribe(res => {
      console.log(res);
      this.isLogin = true;
      this.username = undefined;
      this.password = undefined;
      alert('Regstration success!!!');
    })
  }
}


