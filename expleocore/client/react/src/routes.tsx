import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { DefaultLayout, LoginLayout, LeftNavLayout } from "./layouts";

// Route Views
import Dashboard from "./views/dashboard/Dashboard";
import LoginApp from "./components/login/login";
import Errors from "./views/errors/Errors";
import FundTransfer from "./views/fundTransfer/FundTransfer";
//import ManageBeneficiary from "./views/managebeneficiary/managebeneficiary";
import ManageBeneficiary from "./views/managebeneficiary/manage";
import CustomerCreation from "./views/customerCreation/customerCreation";
//import AddBeneficiary from "./views/addBeneficiary/AddBeneficiary";
// import ChangePassword from "./views/changepassword/changePassword";
// import EditProfile from "./views/editprofile/editProfile";

export default [
  {
    path: "/",
    exact: true,
    layout: DefaultLayout,
    component: () => <Redirect to="/login" />
  },
  {
    path: "/login",
    layout: LoginLayout,
    component: LoginApp
  },
  {
    path: "/home",
    layout: LeftNavLayout,
    component: Dashboard
  },
  // {
  //   path: "/addBenificiary",
  //   layout: LeftNavLayout,
  //   component: AddBeneficiary
  // },
    {
    path: "/customercreation",
    layout: LeftNavLayout,
    component: CustomerCreation
  },
  {
    path: "/managebenificiary",
    layout: LeftNavLayout,
    component: ManageBeneficiary
  },
  {
    path: "/fundtransfer",
    layout: LeftNavLayout,
    component: FundTransfer
  },
  {
    path: "/errors",
    layout: LeftNavLayout,
    component: Errors
  },
  // {
  //   path: "/changepassword",
  //   layout: LeftNavLayout,
  //   component: ChangePassword
  // },
  // {
  //   path: "/editprofile",
  //   layout: LeftNavLayout,
  //   component: EditProfile
  // }
];
