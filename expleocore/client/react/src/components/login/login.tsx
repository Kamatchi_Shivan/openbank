import React from "react";
import "./css/login.css"
import "./css/font-awesome-4.7.0/css/font-awesome.css"
import "./js/api.js"
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import history from '../../history';
import message from '../messages/message.json'
import { loginAction, logoutAction ,FetchAccountSummary ,inmemoryloginDetails } from '../../redux/actions/login/loginAction';
import { Row, Input, Form, Button, Layout, Alert, Select,  Spin, Switch } from 'antd';
import Captcha from "./captcha";
import PropTypes from 'prop-types';
import Global from '../layout/Global/Global';
import i18n, { lang, setInitialLocale } from '../../i18n'
import { locale } from "i18n-js";
import { inmemoryLanguage } from "../../redux/actions/language/languageAction";
import { threadId } from "worker_threads";
import Password from "antd/lib/input/Password";
import { isNull } from "util";
import {config} from "../../config/configs"

const { Option } = Select;

type MyProps = {
  inmemoryLanguage: any,
  logoutAction: any,
  loginAction: any,
  // getMetaData: any,
  loginData: any,
  // metaData: any,
  form: any,
  token: any,
  FetchAccountSummary :any;
  inmemoryloginDetails:any;
  loginDetails: any;
};
type MyState = {
  captchaError: string,
  language: string,
  loading: boolean
};

class Login extends React.Component<MyProps, MyState> {
  // componentWillMount() {
  //   this.props.logoutAction();
  //   if (this.props.token ) {
  //     this.props.FetchAccountSummary(this.props.token);
  //   }
  // }

  constructor(props: any) {
    super(props)
    this.state = {
      captchaError: '',
      language: 'en',
      loading: false
    }
  }

  // handleSelect = (e: any) => {
  //   localStorage.setItem("language", e);
  //   i18n.locale = e;
  //   this.setState({ language: e })
  //   //this.props.inmemoryLanguage(e);
  //   // setInitialLocale(locale(e));

  // };
 

  handleSubmit = (e: any) => {
    e.preventDefault();
    
    (document.getElementById('Demo') as HTMLInputElement).disabled = true;
    
    this.props.form.validateFieldsAndScroll(async (err: any, values: any) => {
      if (!err) {
        console.log("Received values from form",values)
       
        //let recaptcha = (document.forms as any)["myForm"]["g-recaptcha-response"].value;
        this.setState({loading:true});
        localStorage.setItem("token",this.props.token);  
        let response1 = await this.props.loginAction(values,this.props.loginDetails.userName);
        console.log("response1", response1);
        let response2 = await this.props.inmemoryloginDetails(values);
        console.log("response2", response2); 

      }
    });
  };

  renderRedirect = () => {
    if (this.props.token ) {
      this.props.FetchAccountSummary(this.props.token);
    }
    if (this.props.loginData.redirect && this.props.loginDetails) {
      this.setState({loading:false});
      console.log(34534533,this.props.loginDetails.userName,config.adminUser)
      if(this.props.loginDetails.userName!==config.adminUser){
        return <Redirect to='/home' />
      }else{
        return <Redirect to='/customercreation' />
      }
    }
  }

  render() {
    // let title = '';
    // if (this.props.metaData.other_metadata) {
    //   title = this.props.metaData.other_metadata.title;
    // }
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="login-body ">
 <div className="login-layout ">
        <div className="logo-section">
          <div className="languagedropdown">
          </div>
          {/* <img src={require("./images/logo.png")} alt="banner" className="img-responsive" /> */}
        </div>
        <div className="login">
           {this.renderRedirect()} 
          <div className="login-content">
            <div className="login-top-content">
              <h1 className="login-title">{i18n.t(lang.labels["Online"])} <span>{i18n.t(lang.labels["Banking"])}</span></h1>
              {/* <div className="login-para">{i18n.t(lang.labels["access_application_forever"])}</div>
              <div className="login-btn">
                <a href="javascript:void(0)"><span>{i18n.t(lang.buttons["learn_more"])}</span></a>
              </div> */}
            </div>
            <img src={require("./images/loginbanner.png")} alt="banner" className="img-responsive" />
          </div>
          <div className="login-form">
            <div className="login-form-section">
              <h1 className="login-content-title">Open Bank</h1>
              <p>{i18n.t(lang.labels["login_information"])}</p>
              <Form onSubmit={this.handleSubmit} className="form-signin">
                <fieldset id="Demo">
                <div className="form-group mail-box">
                  <i className="fa fa-envelope-o" aria-hidden="true"></i>
                  <div className="form-info">
                    <label>User Name</label>
                    <Form.Item>
                      {getFieldDecorator('userName', {
                        rules: [
                          {
                            message: 'Please enter Username',
                            required: true
                          },
                          {
                            max: 100,
                            message: "User should be accept maximum 35 characters"
                          },
                          {
                            message: "User should be minimum 4 characters",
                            min: 4,
                          }
                        ],
                      })
                        (
                          <Input className="form-control" id="Text" name="Text" placeholder="Enter User Name" allowClear />)}
                    </Form.Item>
                  </div>
                </div>

                <div className="form-group password-box">
                  <i className="fa fa-lock" aria-hidden="true"></i>
                  <div className="form-info">
                    <label htmlFor="password">{i18n.t(lang.labels["password"])}</label>
                    <Form.Item>
                      {getFieldDecorator('password', {
                        rules: [
                          {
                            message: 'Please enter Password',
                            required: true
                          },
                          {
                            max: 25,
                            message: "Password should be accept maximum 25 characters"
                          },
                          {
                            message: "Password should be minimum 5 characters",
                            min: 5
                          }
                        ],
                      })
                        (<Row type="flex" justify="space-between">
                          <Input.Password className="form-control" id="password" name="password" placeholder="Enter password" />
                        </Row>)}
                    </Form.Item>
                  </div>
                </div>
                <div className="loginerror">{this.props.loginData.message}</div> 
                <div className="btn-action">
                  {/* <a href="javascript:void(0);" title="Click to get reset password info" className="login-forget-password">{i18n.t(lang.labels["forgot_password"])}</a> */}
                  <Button className="login-form-btn" htmlType="submit" type="primary" loading={this.state.loading}>
                  {/* <Spin spinning={this.state.loading} delay={500}></Spin> */}
                    {i18n.t(lang.buttons["login_now"])}
                  </Button>
                </div>
                </fieldset>
              </Form>
            </div>
          </div>
        </div>
      </div>
      </div>
      
     
    )
  }
}

const mapStateToProps = (state:any) => ({
  loginData: state.loginData,
  token:state.loginData.token,
  loginDetails:state.loginData.loginDetails
 // accountSummary: state.loginData.accountSummaryDetails, 
 // balance: state.loginData.overall_balance, 
 // account: state.loginData.accounts
 });

const LoginuserForm = Form.create({ name: 'Login' })(Login);

export default connect(mapStateToProps, 
  {
   loginAction,
   logoutAction,
   inmemoryLanguage,
   FetchAccountSummary,
   inmemoryloginDetails
  }
  )(LoginuserForm);
