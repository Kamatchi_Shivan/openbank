import React from "react";
import PropTypes from "prop-types";
import { NavLink as RouteNavLink } from "react-router-dom";
import { NavItem, NavLink } from "shards-react";
import SmileyComponent from "../Global/SmileyComponent";
import Accounts from "../../../assets/images/avatars/account-overview-icon.svg";
import Beneficiary from "../../../assets/images/avatars/beneficiary-icon.svg";
import FundTransfer from "../../../assets/images/avatars/transfer-icon.svg";

const SidebarNavItem = ({ item }: { item: any }) => (
  <NavItem>
    <NavLink tag={RouteNavLink} to={item.to}>
      {item.htmlBefore && (
        <div
          className="d-inline-block item-icon-wrapper"
          dangerouslySetInnerHTML={{ __html: item.htmlBefore }}
        />
      )}
      {item.title && (
        <div className="row">
        <div className = "col-md-1">
          {item.title === "Accounts" ? (
            <img src={Accounts} height="15" alt="" />
          ) : null}
          {item.title === "Add Beneficiary" ? (
            <img src={Beneficiary} height="15" alt="" />
          ) : null}
           {item.title === "Manage Beneficiary" ? (
            <img src={Beneficiary} height="15" alt="" />
          ) : null}
          {item.title === "Fund Transfer" ? (
            <img src={FundTransfer} height="15" alt="" />
          ) : null}
          {item.title === "Customer Creation" ? (
            <img src={Beneficiary} height="15" alt="" />
          ) : null}
          </div>
          <div className = "col-md-1">
          <h5 className="side-menu-text"> {item.title}</h5>
        </div>
        </div>
      )}
      {item.htmlAfter && (
        <div
          className="d-inline-block item-icon-wrapper"
          dangerouslySetInnerHTML={{ __html: item.htmlAfter }}
        />
      )}
    </NavLink>
  </NavItem>
);

SidebarNavItem.propTypes = {
  /**
   * The item object.
   */
  item: PropTypes.object,
};

export default SidebarNavItem;
