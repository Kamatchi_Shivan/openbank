import React from "react";
import { NavNavbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  FormInput,
  Collapse } from "shards-react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {config} from '../../../config/configs'

import SidebarNavItem from "./SidebarNavItem";

type MyProps = {
  loginDetails:any;
};
type MyState = {
  navItems: Array<{
    title: string;
    htmlBefore: string;
    to: string;
  }>;
  adminnavItems: Array<{
    title: string;
    htmlBefore: string;
    to: string;
  }>;
};
class SidebarNavItems extends React.Component<MyProps, MyState> {
  constructor(props: any) {
    super(props);

    this.state = {
      navItems: [
        {
          title: "Accounts",
          htmlBefore: "<i></i>",
          to: "/home",
        },
        // {
        //   title: "Add Beneficiary",
        //   htmlBefore: "<i></i>",
        //   to: "/addBenificiary",
        // },
        {
          title: "Manage Beneficiary",
          htmlBefore: "<i></i>",
          to: "/manageBenificiary",
        },
        {
          title: "Fund Transfer",
          htmlBefore: "<i></i>",
          to: "/fundtransfer",
        },
      ],
      adminnavItems:[
        {
          title: "Customer Creation",
          htmlBefore: "<i></i>",
          to: "/customercreation",
        }
      ]
    };
  }

  render() {
    const { navItems: items,adminnavItems: item } = this.state;
    return (
      <div className="nav-wrapper">
        <img
          className="user-avatar"
          src={require("../../../assets/images/avatars/OpenBank-Logo.png")} /*{this.props.loginData.avatar}*/
          alt=""
        />{" "}
        {
          this.props.loginDetails.userName===config.adminUser?(
            <Nav className="border-left">
          {item.map((item: any, idx: any) => (
            <SidebarNavItem key={idx} item={item} />
          ))}
        </Nav>
          ):(
            <Nav className="border-left">
          {items.map((item: any, idx: any) => (
            <SidebarNavItem key={idx} item={item} />
          ))}
        </Nav>
          )
        }
        
      </div>
    );
  }
}
const mapStateToProps = (state: any) => ({ 
  loginData: state.loginData,
  loginDetails:state.loginData.loginDetails
});

export default connect(mapStateToProps)(SidebarNavItems);

