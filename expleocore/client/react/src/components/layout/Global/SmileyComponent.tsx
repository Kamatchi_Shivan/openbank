import React from "react";

const SmileyComponent = (props) => {
  return <img src={props.img} alt={props.desc} width="30" />;
};

export default SmileyComponent;
