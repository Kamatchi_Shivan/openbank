import React from "react";
import { Link } from "react-router-dom";
import {
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Collapse,
  NavItem,
  NavLink,
} from "shards-react";
import { connect } from "react-redux";
import { Button } from "antd";
import Global from "../../../layout/Global/Global";
import {logoutAction} from "../../../../redux/actions/login/loginAction";

type MyProps = {
  loginData: {
    firstName: string;    
  };
  logoutAction: any;
  accounts: any;
  loginDetails:any;
};
type MyState = { visible: boolean };
class UserActions extends React.Component<MyProps, MyState> {
  constructor(props: any) {
    super(props);

    this.state = {
      visible: false,
    };

    this.toggleUserActions = this.toggleUserActions.bind(this);
  }

  toggleUserActions() {
    this.setState({
      visible: !this.state.visible,
    });
  }

  handleClose = () => {
    this.props.logoutAction();
  }

  render() {
    return (
      <div className="header-section">  
      {this.props.loginDetails?(
        <p className = "headertext-menu-text">Welcome! {this.props.loginDetails.userName}</p>
      ):(
        <p className = "headertext-menu-text">Welcome!</p>
      )}     
        
        <Button type="primary" onClick={this.handleClose}>Logout</Button>
      </div>
    );
  }
}
const mapStateToProps = (state: any) => ({ 
  loginData: state.loginData,
  loginDetails:state.loginData.loginDetails
});
export default connect(mapStateToProps,{logoutAction})(UserActions);
