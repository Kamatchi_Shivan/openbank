export const config = {
    environment: process.env.REACT_APP_NODE_ENV,
    companyName: 'Expleo',
    secretKey:process.env.REACT_APP_SECRET_KEY,
    appBaseName:process.env.REACT_APP_BASENAME,
    appGAID:process.env.REACT_APP_GAID,
    captcha:"google",
    // apiRootPath: process.env.REACT_APP_API_ROOT_PATH || "https://apisandbox.openbankproject.com",
    apiRootPath: process.env.REACT_APP_API_ROOT_PATH ||"https://cors-anywhere.herokuapp.com/https://apisandbox.openbankproject.com",
    authenticationURL:"/my/logins/direct",
    adminUser:"bankadmin",
    BANK_ID:"in-bank-x-1"
    // getMetaDataURL:"/init/getMetaData",
    // websocketRootPath:process.env.REACT_APP_WEBSOCKET_ROOT_PATH || "ws://localhost:8000/",
    // websocketURL:"ws://localhost:8000/api/ws/notification",
    // changePasswordURL:"/v1/updatePassword",
    // updateUserURL:"/v1/updateUser/"
};
