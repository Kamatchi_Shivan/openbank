import React, { Component } from "react";
import { connect } from "react-redux";
import { Container } from "shards-react";
import { Row, Input, Form, Button, Alert, Card, Col } from "antd";
import Global from "../../components/layout/Global/Global";
import Popup from "reactjs-popup";
import { logoutAction, FetchAccountSummary } from '../../redux/actions/login/loginAction';
import { CounterPartiesCreation } from "../../redux/actions/AddBeneficiary/addBeneficiaryAction";
import {
  setGlobalMessage,
  clearGlobalMessage,
} from "../../redux/actions/messageHandlerAction";
import Error from "../../assets/images/avatars/Error.svg";
import Success from "../../assets/images/avatars/tick-icon.svg"

type MyProps = {
  form: any;
  CounterPartiesCreation: any;
  accountSummary: any;
  balance: any;
  account: any;
  token: any;
  globalMessage: any;
  clearGlobalMessage: any;
  FetchAccountSummary: any;
};

type MyState = {
  popState: boolean;
  consversionPanel: boolean;
};
class AddBeneficiary extends Component<MyProps, MyState> {
  componentWillMount() {
    this.props.clearGlobalMessage();
  }
  constructor(props: any) {
    super(props);
    this.state = {
      consversionPanel: true,
      popState: true,
    };
  }

  handleSubmit = (e: any) => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err: any, values: any) => {
      if (!err) {
       // if (values.accountNumber === values.CaccountNumber) {
          let loginData = {
            token: this.props.token,
            accountId: this.props.accountSummary[0].id,
          };
          console.log("Values---->", loginData, values);
          this.setState({ popState: true });
          this.props.CounterPartiesCreation(values, loginData);
       // } else {
        //   alert("Confirm Account number is not same as the Account Number");
        // }
      }
    });
  };

  // clearGlobalmessage = () => {
  //   this.props.clearGlobalMessage();
  // };

  // handleClose() {
  //   Global.history.push("/home");
  // }

  closeModal = () => {
    if (this.props.token) {
      this.props.FetchAccountSummary(this.props.token);
    }
    this.props.clearGlobalMessage();
    this.setState({ popState: false });
    Global.history.push("/home");
  }

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('accountNumber')) {
      callback('Confirm Account number is not same as the Account Number');
    } else {
      callback();
    }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form>
        <h5>
          <b>Add Beneficiary </b>
        </h5>
        <div className="container-fluid">
          <Row type="flex" justify="center">
            <Col span={8}>
              {this.props.globalMessage.status ? (
                this.props.globalMessage.status === "SUCCESS" ||
                  this.props.globalMessage.status === "success" ? (
                    <Popup
                      open={this.state.popState}
                      closeOnDocumentClick
                      onClose={this.closeModal}
                    >
                      <div className="pop-up">
                        <a className="close" onClick={this.closeModal}>
                          &times;
                      </a>
                        <img className="popup-icon" src={Success} alt="Success" />
                        <h5 className="pop-text">{this.props.globalMessage.message}</h5>
                      </div>
                    </Popup>
                  ) : (
                    <Popup
                      open={this.state.popState}
                      closeOnDocumentClick
                      onClose={this.closeModal}
                    >
                      <div className="pop-up">
                        <a className="close" onClick={this.closeModal}>
                          &times;
                      </a>
                        <img className="popup-icon" src={Error} alt="Error" />
                        <h5 className="pop-text">{this.props.globalMessage.message}</h5>
                      </div>
                    </Popup>
                  )
              ) : null}
            </Col>
          </Row>
          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6">
                  <Form.Item>
                    {getFieldDecorator("name", {
                      rules: [
                        {
                          message: "Please enter name",
                          required: true,
                        },
                        {
                          max: 100,
                          message:
                            "name should be accept maximum 100 characters",
                        },
                        {
                          message: "name should be minimum 2 characters",
                          min: 2,
                        },
                      ],
                    })(<Input placeholder="Name" />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item>
                    {getFieldDecorator("description", {
                      rules: [
                        {
                          message: "Please enter Description",
                          required: true,
                        },
                        {
                          max: 1000,
                          message:
                            "Description should be accept maximum 1000 characters",
                        },
                        {
                          message: "Description should be minimum 2 characters",
                          min: 2,
                        },
                      ],
                    })(<Input placeholder="Description" />)}
                  </Form.Item>
                </div>
                <div className="col-md-6 col-sm-12">
                  <Form.Item
                    hasFeedback>
                    {getFieldDecorator("accountNumber", {
                      rules: [
                        {
                          message: "Please enter Account Number",
                          required: true,
                        },
                        {
                          max: 100,
                          message:
                            "Account Number should be accept maximum 100 characters",
                        },
                        {
                          message:
                            "Account Number should be minimum 10 characters",
                          min: 10,
                        },
                      ],
                    })(<Input placeholder="Account Number" />)}
                  </Form.Item>
                </div>
                <div className="col-md-6 col-sm-12">
                  <Form.Item
                    hasFeedback
                  >
                    {getFieldDecorator("CaccountNumber", {

                      rules: [
                        {
                          required: true, message: 'Please confirm your AccountNumber!',
                        }, {
                          validator: this.compareToFirstPassword,
                        }
                      ],

                    })(<Input placeholder="Confirm Account Number" />)}
                  </Form.Item>

                </div>
                <div className="col-md-12 btn-align">
                  <Button type="primary" onClick={this.handleSubmit}>
                    Add
                  </Button>
                </div>
              </div>
            </Card>
          </div>
        </div>
      </Form>
    );
  }
}

const mapStateToProps = (state: any) => ({
  accountSummary: state.loginData.accountSummaryDetails.accounts,
  balance: state.loginData.accountSummaryDetails.overall_balance,
  globalMessage: state.globalMessage,
  token: state.loginData.token,
});

const AddBeneficiaryForm = Form.create({ name: "AddBeneficiary" })(
  AddBeneficiary
);

export default connect(mapStateToProps, {
  CounterPartiesCreation,
  setGlobalMessage,
  clearGlobalMessage,
  FetchAccountSummary
})(AddBeneficiaryForm);
