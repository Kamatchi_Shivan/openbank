import React, { Component } from "react";
import { connect } from 'react-redux';
import { Container } from "shards-react";
//import '../../views/dashboard/dashboard.css';
// import "antd/dist/antd.css";
import { Row, Input, Form, Button, Layout, Alert, Select, Table } from 'antd';
import { logoutAction, FetchAccountSummary } from '../../redux/actions/login/loginAction';
import { FetchCounterParties } from '../../redux/actions/FundTransfer/fundTransferAction';


type MyProps = {
  logoutAction: any,
  loginData: any,
  accountSummary: any,
  balance: any,
  account: any,
  FetchAccountSummary: any,
  token: any,
  dataSource: any,
  columns: any,
  FetchCounterParties: any,
  accounts:any
};

const columns = [
  {
    title: 'Name',
    dataIndex: 'name',
    key: 'name',
    width: 200,
  },
  {
    title: 'Account Number',
    dataIndex: 'account_no',
    key: 'account_no',
    width: 200,
  },
  {
    title: 'Bank',
    dataIndex: 'bankname',
    key: 'bankname',
    width: 200,
  },
  {
    title: 'Amount',
    dataIndex: 'amount',
    key: 'amount',
    width: 200,
  }]

class Dashboard extends Component<MyProps> {

  componentWillMount() {
    if (this.props.token!==null && this.props.token!==undefined && this.props.accounts.length!==0 ) {
      let loginData = {
        token : this.props.token,
        accountId : this.props.accounts[0].id
      }
      this.props.FetchCounterParties(loginData);
    }
  }
 
  constructor(props: any) {
    super(props);
    this.state = {
      consversionPanel: true
    }
  }
  

  render() {
    const data = [];
    if (Object.keys(this.props.accountSummary).length !== 0 ) {
      if(this.props.accountSummary.accounts.length!==0){
        data.push({
          name: this.props.accountSummary.accounts[0].label,
          account_no: this.props.accountSummary.accounts[0].id,
          bankname: this.props.accountSummary.accounts[0].bank_id,
          amount: this.props.accountSummary.overall_balance.currency+" "+this.props.accountSummary.overall_balance.amount
        })
      }
     
    }

    return (
      <div>
        <div className="header-text" >
             Account Summary
          </div>
          <div>
              <Table
              dataSource={data} columns={columns} bordered pagination={false} className="table-striped-rows"
                rowKey="account_no" />
           </div>
      </div>
          
        
    )
  };
};

const mapStateToProps = (state: any) =>
  ({
    accountSummary: state.loginData.accountSummaryDetails,
    accounts: state.loginData.accountSummaryDetails.accounts,
    token: state.loginData.token
  });

export default connect(mapStateToProps, { FetchAccountSummary, logoutAction ,FetchCounterParties})(Dashboard);