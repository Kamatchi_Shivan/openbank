import React, { Component } from "react";
import { connect } from "react-redux";
import { Container } from "shards-react";
import { Row, Input, Form, Button, Alert, Card, Col, Checkbox, InputNumber } from "antd";
import Global from "../../components/layout/Global/Global";
import Popup from "reactjs-popup";
import {
    setGlobalMessage,
    clearGlobalMessage,
} from "../../redux/actions/messageHandlerAction";
import { loginAction } from '../../redux/actions/login/loginAction';
import { CreateUser, CreateAccount } from '../../redux/actions/customerCreation/customerCreateAction'
import { transactionRequest } from '../../redux/actions/FundTransfer/fundTransferAction'
import Error from "../../assets/images/avatars/Error.svg";
import Success from "../../assets/images/avatars/tick-icon.svg";
import message from '../../components/messages/message.json';

type MyProps = {
    form: any;
    CreateUser: any;
    createUser: any;
    loginData: any;
    globalMessage: any;
    loginAction: any;
    CreateAccount: any;
    transactionRequest: any;
    createAccount: any;
    loginDetails: any;
    usertoken: any;
    clearGlobalMessage: any;
    loading: any;

};

type MyState = {
    popState: boolean;
    loading: boolean;
};

class CustomerCreation extends Component<MyProps, MyState> {
    componentWillMount() {
        //this.props.clearGlobalMessage();
    }
    constructor(props: any) {
        super(props);
        this.state = {
            popState: false,
            loading: false

        };
    }

    handleSubmit = (e: any) => {
        e.preventDefault();
        (document.getElementById('Demo') as HTMLInputElement).disabled = true;

        this.props.form.validateFieldsAndScroll(async (err: any, values: any) => {
            if (!err) {
                console.log('Received values of form: ', values);
                this.setState({ loading: true })

                let response1 = await this.props.CreateUser(values, this.props.loginData.token);
                console.log("response1", response1);
                if (response1 !== "error" && response1.status === 201) {
                    let response2 = await this.props.loginAction(values, this.props.loginDetails.userName);
                    console.log("response2", response2);
                    if (response2 !== "error" && response2.status === 201) {
                        let response3 = await this.props.CreateAccount(values, this.props.createUser, this.props.loginData.token);
                        console.log("response3", response3);
                        if (response3 !== "error" && response3.status === 201) {
                            let balancevalue = {
                                beneficiary_name: this.props.createAccount.account_id,
                                amount: values.amount,
                                description: "basic balance"
                            }
                            let data = {
                                token: this.props.usertoken.token,
                                accountId: this.props.createAccount.account_id
                            }
                            let response4 = await this.props.transactionRequest(balancevalue, data);
                            console.log("response4", response4);
                            if (response4 !== "error" && response4.status === 201) {
                                this.setState({ popState: true });
                            }
                            else { this.setState({ popState: true }); }
                        }
                        else { this.setState({ popState: true }); }
                    }
                    else { this.setState({ popState: true }); }
                }
                else { this.setState({ popState: true }); }
            }
        });
    };



    closeModal = () => {
        this.props.clearGlobalMessage();
        // this.props.form.resetFields();
        this.setState({ popState: false, loading: false });
        (document.getElementById('Demo') as HTMLInputElement).disabled = false;
    }

    compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('Confirm Password is not same as the Password');
        } else {
            callback();
        }
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <Form>
                <div className="header-text" >
                    Customer Creation
          </div>
                <div className="container-fluid">
                    <Row type="flex" justify="center">
                        <Col span={8}>
                            {this.props.globalMessage.status ? (
                                this.props.globalMessage.status === "SUCCESS" ||
                                    this.props.globalMessage.status === "success" ? (
                                        <Popup
                                            open={this.state.popState}
                                            closeOnDocumentClick
                                            onClose={this.closeModal}
                                        >
                                            <div className="pop-up">
                                                <a className="close" onClick={this.closeModal}>
                                                    &times;
                      </a>
                                                <img className="popup-icon" src={Success} alt="Success" />
                                                <h5 className="pop-text">{message.CreationSuccessMessage}</h5>
                                            </div>
                                        </Popup>
                                    ) : (
                                        <Popup
                                            open={this.state.popState}
                                            closeOnDocumentClick
                                            onClose={this.closeModal}
                                        >
                                            <div className="pop-up">
                                                <a className="close" onClick={this.closeModal}>
                                                    &times;
                      </a>
                                                <img className="popup-icon" src={Error} alt="Error" />
                                                <h5 className="pop-text">{message.CreationErrorMessage}</h5>
                                            </div>
                                        </Popup>
                                    )
                            ) : null}
                        </Col>
                    </Row>
                    <fieldset id="Demo">
                        <div className="cardheader">
                            <Card className="antcardborder">
                                <div className="row">
                                    <div className="col-md-6">
                                        <Form.Item label="First Name">
                                            {getFieldDecorator("fname", {
                                                rules: [
                                                    {
                                                        message: "Please enter First Name",
                                                        required: true,
                                                    },
                                                    {
                                                        max: 100,
                                                        message:
                                                            "First Name should be accept maximum 100 characters",
                                                    },
                                                    {
                                                        message: "First Name should be minimum 2 characters",
                                                        min: 2,
                                                    },
                                                ],
                                            })(<Input />)}
                                        </Form.Item>
                                    </div>

                                    <div className="col-md-6 col-sm-12">
                                        <Form.Item label="Last Name">
                                            {getFieldDecorator("lname", {
                                                rules: [
                                                    {
                                                        message: "Please enter Last Name",
                                                        required: true,
                                                    },
                                                    {
                                                        max: 100,
                                                        message:
                                                            "Last Name should be accept maximum 100 characters",
                                                    },
                                                    {
                                                        message: "Last Name should be minimum 2 characters",
                                                        min: 2,
                                                    },
                                                ],
                                            })(<Input />)}
                                        </Form.Item>
                                    </div>

                                    <div className="col-md-6 col-sm-12">
                                        <Form.Item label="Email">
                                            {getFieldDecorator("email", {
                                                rules: [
                                                    {
                                                        type: 'email',
                                                        message: 'The input is not valid E-mail!',
                                                    },
                                                    {
                                                        required: true,
                                                        message: 'Please input your E-mail!',
                                                    },
                                                ]
                                            })(<Input />)}
                                        </Form.Item>
                                    </div>

                                    <div className="col-md-6 col-sm-12">
                                        <Form.Item label="Username">
                                            {getFieldDecorator("userName", {
                                                rules: [
                                                    {
                                                        message: "Please enter Username",
                                                        required: true,
                                                    },
                                                    {
                                                        max: 100,
                                                        message:
                                                            "Username should be accept maximum 100 characters",
                                                    },
                                                    {
                                                        message:
                                                            "Username should be minimum 10 characters",
                                                        min: 2,
                                                    },
                                                ],
                                            })(<Input />)}
                                        </Form.Item>
                                    </div>

                                    <div className="col-md-6 col-sm-12">

                                        <Form.Item label="Password" hasFeedback>
                                            {getFieldDecorator("password", {
                                                rules: [
                                                    {
                                                        required: true,
                                                        message: 'Please input your password!',
                                                        /**Your password should EITHER be at least 10 characters
                                                         * long and contain mixed numbers and both upper and lower 
                                                         * case letters and at least one special character, 
                                                         * OR be longer than 16 characters */
                                                    },
                                                    {
                                                        message:
                                                            "Password should be minimum 5 characters",
                                                        min: 5,
                                                    },
                                                ],
                                            })(<Input />)}
                                        </Form.Item>
                                    </div>

                                    <div className="col-md-6 col-sm-12">
                                        <Form.Item label="ConfirmPassword"
                                            hasFeedback
                                        >
                                            {getFieldDecorator("ConfirmPassword", {

                                                rules: [
                                                    {
                                                        required: true,
                                                        message: 'Please confirm your Password!',
                                                    },
                                                    {
                                                        validator: this.compareToFirstPassword,
                                                    }
                                                ],

                                            })(<Input />)}
                                        </Form.Item>
                                    </div>

                                    <div className="col-md-6 col-sm-12">
                                        <Form.Item label="Amount">
                                            {getFieldDecorator("amount", {
                                                rules: [
                                                    {
                                                        required: true,
                                                        message: "Please enter amount",
                                                    },
                                                    // {
                                                    //   max: 100,
                                                    //   message:
                                                    //     "amount should be accept maximum 100 characters"
                                                    // },
                                                    {
                                                        type: 'number',
                                                        min: 0,
                                                        message: "Please Enter Valid Amount",
                                                    }
                                                ]
                                            })(<InputNumber />)}
                                        </Form.Item>
                                    </div>

                                    <div className="col-md-12 btn-align">
                                        <Button type="primary" onClick={this.handleSubmit} loading={this.state.loading}>
                                            Submit
                                    </Button>
                                    </div>
                                </div>
                            </Card>
                        </div>
                    </fieldset>
                </div>
            </Form>
        );
    }
}

const mapStateToProps = (state: any) => ({
    createUser: state.customerCreation.createUser,
    createAccount: state.customerCreation.createAccount,
    globalMessage: state.globalMessage,
    loginData: state.loginData,
    loginDetails: state.loginData.loginDetails,
    usertoken: state.customerCreation.usertoken
});

const CustomerCreationForm = Form.create({ name: "CustomerCreation" })(
    CustomerCreation
);

export default connect(mapStateToProps, {
    setGlobalMessage,
    clearGlobalMessage,
    CreateUser,
    loginAction,
    CreateAccount,
    transactionRequest
})(CustomerCreationForm);
