import React, { Component } from "react";
import { connect } from "react-redux";
import { Container } from "shards-react";
import { Row, Input, Form, Button, Alert, Card, Col ,Table, Modal } from "antd";
import Global from "../../components/layout/Global/Global";
import { logoutAction, FetchAccountSummary } from '../../redux/actions/login/loginAction';
import { CounterPartiesCreation } from "../../redux/actions/AddBeneficiary/addBeneficiaryAction";
import {
  setGlobalMessage,
  clearGlobalMessage,
} from "../../redux/actions/messageHandlerAction";
import Error from "../../assets/images/avatars/Error.svg";
import Success from "../../assets/images/avatars/tick-icon.svg"
//import ModalForm from "./modelform";

let statevariable=false;
let temp=0

type MyProps = {
    form: any;
    CounterPartiesCreation: any;
    accountSummary: any;
    balance: any;
    account: any;
    token: any;
    globalMessage: any;
    clearGlobalMessage: any;
    FetchAccountSummary: any;
    counterparties: any;
    //removeColumnVisibility:any;
    onCancel: any;
    onCreate:any;
    propColumn:any;
    
  };
  
  type MyState = {
    popState: boolean;
    consversionPanel: boolean;
    columns:any;
    visible:boolean;
    removeColumnVisibility:boolean;
   // modalVisible:boolean
  }; 

  type MyState1 = {      
    modalVisible:boolean
  }; 

  type MyProps1 = {   
    removeColumnVisibility:any
    onCancel:any
    onCreate:any
    form:any
    propColumn:any
    visible   :any
   
  }; 

  // const ModalFormComponent = ({ visible, onCancel, onCreate, form }) => {

  //   const{wrappedComponentRef}=this.props
 
  //    const { getFieldDecorator } = form;
const BeneficiaryModal = Form.create({ name: "form" })(
    
  class extends React.Component<any,MyState1,MyProps1> {
      
    constructor(props) {
        super(props);
        this.state = {
          modalVisible:true        
        }; 
    }

    handleSubmit = (e: any) => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err: any, values: any) => {
          if (!err) {
           // if (values.accountNumber === values.CaccountNumber) {
              let loginData = {
                token: this.props.token,
                accountId: this.props.accountSummary[0].id,
              };
             // this.setState({ popState: true });
              this.props.CounterPartiesCreation(values, loginData);
           // } else {
            //   alert("Confirm Account number is not same as the Account Number");
            // }
          }
        });
      };

      compareToFirstPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('accountNumber')) {
          callback('Confirm Account number is not same as the Account Number');
        } else {
          callback();
        }
      }

    //   handleCancelled = () => {
    //    this.setState({visible:false})        
    // }
    // handleAdd = () => {
    //     // this.setState({
    //     //   ModalText: 'The modal will be closed after two seconds',
    //     //   confirmLoading: true,
    //     // });
    //     setTimeout(() => {
    //       this.setState({
    //         visible: false,
    //        // confirmLoading: false,
    //       });
    //     }, 2000);
    //   };
    
      handleCancel = () => {
        statevariable=false
        // this.setState({
        //     modalVisible: false,
        //    // visible:false
        // });
        temp=1
      };
    
    render() {
        const {
            removeColumnVisibility,
            onCancel,
            onCreate,
            form,
            propColumn,
            visible
            // onDelete
          } = this.props;

         // let modalVisible=true;
    const { getFieldDecorator } = this.props.form;
      return (
        <Modal
          visible={statevariable}
        // visible={removeColumnVisibility}
        //  title="Add Beneficiary"
          okText="Add"
         // cancelText="Cancel"
         // column={this.modalcolumns}
         //confirmLoading={confirmLoading}
         onCancel={this.handleCancel}
        // onCancel={onCancel}
         onOk={onCreate}
          className="cross-model"
        //  maskClosable={false}
          // onDelete= {this.handleTestRunDelete(testArray)}
        //  okButtonProps={{ disabled: this.state.hasDisable }}
        >
          <div className="container-fluid">
            <div className="Breadcrumb-top">
            <Form>
        <h5>
          <b>Add Beneficiary </b>
        </h5>
        <div className="container-fluid">
      
          <div className="cardheader">
            <Card className="antcardborder">
              <div className="row">
                <div className="col-md-6">
                  <Form.Item>
                    {getFieldDecorator("name", {
                      rules: [
                        {
                          message: "Please enter name",
                          required: true,
                        },
                        {
                          max: 100,
                          message:
                            "name should be accept maximum 100 characters",
                        },
                        {
                          message: "name should be minimum 2 characters",
                          min: 2,
                        },
                      ],
                    })(<Input placeholder="Name" />)}
                  </Form.Item>
                </div>

                <div className="col-md-6 col-sm-12">
                  <Form.Item>
                    {getFieldDecorator("description", {
                      rules: [
                        {
                          message: "Please enter Description",
                          required: true,
                        },
                        {
                          max: 1000,
                          message:
                            "Description should be accept maximum 1000 characters",
                        },
                        {
                          message: "Description should be minimum 2 characters",
                          min: 2,
                        },
                      ],
                    })(<Input placeholder="Description" />)}
                  </Form.Item>
                </div>
                <div className="col-md-6 col-sm-12">
                  <Form.Item
                    hasFeedback>
                    {getFieldDecorator("accountNumber", {
                      rules: [
                        {
                          message: "Please enter Account Number",
                          required: true,
                        },
                        {
                          max: 100,
                          message:
                            "Account Number should be accept maximum 100 characters",
                        },
                        {
                          message:
                            "Account Number should be minimum 10 characters",
                          min: 10,
                        },
                      ],
                    })(<Input placeholder="Account Number" />)}
                  </Form.Item>
                </div>
                <div className="col-md-6 col-sm-12">
                  <Form.Item
                    hasFeedback
                  >
                    {getFieldDecorator("CaccountNumber", {

                      rules: [
                        {
                          required: true, message: 'Please confirm your AccountNumber!',
                        }, {
                          validator: this.compareToFirstPassword,
                        }
                      ],

                    })(<Input placeholder="Confirm Account Number" />)}
                  </Form.Item>

                </div>
                <div className="col-md-12 btn-align">
                  <Button type="primary" onClick={this.handleSubmit}>
                    Add
                  </Button>
                </div>
              </div>
            </Card>
          </div>
        </div>
      </Form>
            </div>
          </div>
        </Modal>
      );
    }
  }
);

  class ManageBeneficiary extends Component<MyProps, MyState ,MyState1> {
      formRef: any;
     // removeColumnVisibility: any;
      
    componentWillMount() {
      this.props.clearGlobalMessage();
    }
    
    constructor(props: any) {
      super(props);
      this.state = {
        removeColumnVisibility: false,
        consversionPanel: true,
        visible: false,
        popState: true,
       // modalVisible:true,
        
        columns:[{
            dataIndex: "rowIndex",
            editable: false,
            key: "rowIndex",
            render: (text, record, index) => {
              return <div>{++index}</div>;
            },
            title: "S.No ",
            width: 200
        },{
            dataIndex: "name",
            // editable: true,
            key: "name ",
            title: "Beneficiary Name",
            width: 200,
        },{
            dataIndex: "description",
           // editable: true,
            key: "description",
            title: "Description ",
            width: 200            
        },
        {
            title: 'Account Number',
            dataIndex: 'account_no',
            key: 'account_no',
            width: 200,
          }]
      };
    }
    
    showModal = () => {  
      if(temp===0){
        statevariable=true
      // {<BeneficiaryModal/>}
        this.setState({
          visible: true,
        });
      }else{
        temp=0;
        this.setState({
          visible: false,
        });
      }
      };

      // statecontrol=() =>{
      //   if(statevariable===false){
      //     this.setState({ visible: true }); 
      //   }
      //   }

      handleClick=() =>{
        this.setState({ removeColumnVisibility: true });        
      }

      saveFormRef = formRef => {
        this.formRef = formRef;
      };

      handleCancelled = () => {
        this.setState({ visible: false });        

    }
      // handleAddResult = () => {
      //  const { form } = this.form.Ref.props;
      //  form.validateFields((err, values) => {
      //     this.props.getSingleTestRun(testArray[0]._id);
      //     selectionEnabledIn = false;
      //     if (deleteFlag === true && deleteArrayData.length > 0) {
      //       this.props.deleteTestRunList(deleteArrayData);
      //     }
      //     //deleteFlag = false;
      //     Global.history.push("/result");
      //  });
      // };

    render() {
        const data = [];
    
        if (this.props.counterparties.length !== 0) {
            this.props.counterparties.forEach(counterparties => {
          data.push({
            name: counterparties.name,
            description: counterparties.description,
            account_no: counterparties.other_account_routing_address,
          })
        });
        }
    
        return (
            <div className="container-fluid">
                <Row className="margintop">
                    <div className="float-left">
                        <h5 style={{ marginTop: 4 }}>Account Summary</h5>
                    </div>
                    <Button className="buttonleft" onClick={this.showModal}>
                        Add Beneficiary
                        {
                        statevariable?(<BeneficiaryModal 
                        //  removeColumnVisibility={this.state.removeColumnVisibility} 
                         // Onvisible={()=>this.state.visible}  
                                        
                        />):(null)
                    }
                    </Button>

                  {/* <Button className="buttonleft" onClick={()=>this.showModal}>
                    Add Beneficiary
                  </Button> 

                  <ModalForm
                     wrappedComponentRef={this.saveFormRef}
                    // removeColumnVisibility={this.state.removeColumnVisibility}
                     visible={this.state.visible}
                     onCancel={this.handleCancelled}
                     onCreate={this.handleAddResult}
                  /> */}
                 
                </Row>
                <Table
                  dataSource={data} columns={this.state.columns} bordered pagination={false} 
                    rowKey="rowIndex" />
            </div>               
       )
      };
    };

const mapStateToProps = (state: any) => ({
    counterparties:state.fundtransfer.counterparties,
  accountSummary: state.loginData.accountSummaryDetails.accounts,
  balance: state.loginData.accountSummaryDetails.overall_balance,
  globalMessage: state.globalMessage,
  token: state.loginData.token,
});

const ManageBeneficiaryForm = Form.create({ name: "ManageBeneficiary" })(
    ManageBeneficiary
);

export default connect(mapStateToProps, {
  CounterPartiesCreation,
  setGlobalMessage,
  clearGlobalMessage,
  FetchAccountSummary
})(ManageBeneficiaryForm);
