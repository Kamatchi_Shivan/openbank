import React, { Component } from "react";
import { connect } from "react-redux";
import { Row, Input, Form, Button, Card, Col, Table, Modal } from "antd";
import { FetchAccountSummary } from '../../redux/actions/login/loginAction';
import { CounterPartiesCreation } from "../../redux/actions/AddBeneficiary/addBeneficiaryAction";
import {
  setGlobalMessage,
  clearGlobalMessage,
} from "../../redux/actions/messageHandlerAction";
import { FetchCounterParties } from '../../redux/actions/FundTransfer/fundTransferAction';
import Error from "../../assets/images/avatars/Error.svg";
import Success from "../../assets/images/avatars/tick-icon.svg"
import Popup from "reactjs-popup";

// let statevariable=false;
// let temp=0

const BeneficiaryModal = Form.create({ name: "form_in_modal" })(
  // eslint-disable-next-line
  class extends React.Component {
    state = {
      element: this.props.element,

    };

    compareToFirstPassword = (rule, value, callback) => {
      const form = this.props.form;
      if (value && value !== form.getFieldValue('accountNumber')) {
        callback('Confirm Account number is not same as the Account Number');
      } else {
        callback();
      }
    }

    render() {
      const { visible, onCancel, onCreate, form, confirmLoading } = this.props;
      const { getFieldDecorator } = form;
      return (
        <Modal
          visible={visible}
          title="Add Beneficiary"
          okText="Add"
          cancelText="cancel"
          confirmLoading={confirmLoading}
          onCancel={onCancel}
          onOk={onCreate}
        >
          <div className="container-fluid">
            <div className="Breadcrumb-top">
              <Form>

                <div className="container-fluid">

                  <div className="cardheader">
                    <Card className="antcardborder">
                      <div className="row">
                        <div className="col-md-6">
                          <Form.Item>
                            {getFieldDecorator("name", {
                              rules: [
                                {
                                  message: "Please enter name",
                                  required: true,
                                },
                                {
                                  max: 100,
                                  message:
                                    "name should be accept maximum 100 characters",
                                },
                                {
                                  message: "name should be minimum 2 characters",
                                  min: 2,
                                },
                              ],
                            })(<Input placeholder="Name" />)}
                          </Form.Item>
                        </div>

                        <div className="col-md-6 col-sm-12">
                          <Form.Item>
                            {getFieldDecorator("description", {
                              rules: [
                                {
                                  message: "Please enter Description",
                                  required: true,
                                },
                                {
                                  max: 1000,
                                  message:
                                    "Description should be accept maximum 1000 characters",
                                },
                                {
                                  message: "Description should be minimum 2 characters",
                                  min: 2,
                                },
                              ],
                            })(<Input placeholder="Description" />)}
                          </Form.Item>
                        </div>
                        <div className="col-md-6 col-sm-12">
                          <Form.Item
                            hasFeedback>
                            {getFieldDecorator("accountNumber", {
                              rules: [
                                {
                                  message: "Please enter Account Number",
                                  required: true,
                                },
                                {
                                  max: 100,
                                  message:
                                    "Account Number should be accept maximum 100 characters",
                                },
                                {
                                  message:
                                    "Account Number should be minimum 10 characters",
                                  min: 10,
                                },
                              ],
                            })(<Input placeholder="Account Number" />)}
                          </Form.Item>
                        </div>
                        <div className="col-md-6 col-sm-12">
                          <Form.Item
                            hasFeedback
                          >
                            {getFieldDecorator("CaccountNumber", {

                              rules: [
                                {
                                  message: 'Please confirm your AccountNumber!',
                                  required: true,
                                }, {
                                  validator: this.compareToFirstPassword,
                                }
                              ],

                            })(<Input placeholder="Confirm Account Number" />)}
                          </Form.Item>

                        </div>
                        <div className="col-md-12 btn-align">
                          {/* <Button type="primary" onClick={this.handleSubmit}>
                    Add
                  </Button> */}
                        </div>
                      </div>
                    </Card>
                  </div>
                </div>
              </Form>
            </div>
          </div>
        </Modal>
      );
    }
  }
);

class ManageBeneficiary extends Component {


  componentWillMount() {
    this.props.clearGlobalMessage();
  }

  constructor(props) {
    super(props);
    this.state = {
      removeColumnVisibility: false,
      consversionPanel: true,
      visible: false,
      confirmLoading: false,
      popState: true,

      columns: [
        {
          title: "S.No",
          dataIndex: "serialNumber",
          key: "serialNumber",
          width: "10%"
        },
        {
          dataIndex: "name",
          // editable: true,
          key: "name ",
          title: "Beneficiary Name",
          width: 200,
        }, {
          dataIndex: "description",
          // editable: true,
          key: "description",
          title: "Description ",
          width: 200
        },
        {
          title: 'Account Number',
          dataIndex: 'account_no',
          key: 'account_no',
          width: 200,
        }]
    };
  }

  showModal = () => {
    this.setState({ visible: true });
  };
  handleCancelled = () => {
    this.setState({ visible: false });
    const { form } = this.formRef.props;
    form.resetFields();
  };

  handleClick = () => {
    this.setState({ removeColumnVisibility: true });
  }

  saveFormRef = (formRef) => {
    this.formRef = formRef;
  };


  handleCreate = () => {

    const { form } = this.formRef.props;
    form.validateFields(async (err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
        let loginData = {
          token: this.props.token,
          accountId: this.props.accountSummary[0].id,
        };
        this.setState({ confirmLoading: true });
        let response1 = await this.props.CounterPartiesCreation(values, loginData);
        console.log("response1", response1);
        if (response1 !== "error" && response1.status === 201) {
          let response2 = await this.props.FetchCounterParties(loginData);
          console.log("response2", response2);


          this.setState({ visible: false, confirmLoading: false, popState: true });
          form.resetFields();

        };
      }
    });
  }

  closeModal = () => {
    this.props.clearGlobalMessage();
    this.setState({ popState: false });
    // let loginData = {
    //   token: this.props.token,
    //   accountId: this.props.accountSummary[0].id,
    // };
    // this.props.FetchCounterParties(loginData);
    // if(this.props.globalMessage.status){
    //   this.FetchAccountSummary();
    // }
  }

  render() {

    const data = [];

    if (this.props.counterparties.length !== 0) {
      let SerialNumber = 1;
      this.props.counterparties.forEach(counterparties => {
        data.push({
          serialNumber: SerialNumber,
          name: counterparties.name,
          description: counterparties.description,
          account_no: counterparties.other_account_routing_address,
        })
        SerialNumber++;
      });
    }

    return (
      <div className="container-fluid">
        <Row className="margintop">
          <div className="header-text">
            <div>Manage Beneficiary </div>
            <Button className="buttonleft " onClick={this.showModal}>
              Add Beneficiary
                    </Button>
          </div>

          <Row type="flex" justify="center">
            <Col span={8}>
              {this.props.globalMessage.status ? (
                this.props.globalMessage.status === "SUCCESS" ||
                  this.props.globalMessage.status === "success" ? (
                    <Popup
                      open={this.state.popState}
                      closeOnDocumentClick
                      onClose={this.closeModal}
                    >
                      <div className="pop-up">
                        <a className="close" onClick={this.closeModal}>
                          &times;
                      </a>
                        <img className="popup-icon" src={Success} alt="Success" />
                        <h5 className="pop-text">{this.props.globalMessage.message}</h5>
                      </div>
                    </Popup>
                  ) : (
                    <Popup
                      open={this.state.popState}
                      closeOnDocumentClick
                      onClose={this.closeModal}
                    >
                      <div className="pop-up">
                        <a className="close" onClick={this.closeModal}>
                          &times;
                      </a>
                        <img className="popup-icon" src={Error} alt="Error" />
                        <h5 className="pop-text">{this.props.globalMessage.message}</h5>
                      </div>
                    </Popup>
                  )
              ) : null}
            </Col>
          </Row>
          <BeneficiaryModal
            //   wrappedComponentRef={this.saveFormRef}
            //  // ref={this.saveFormRef}
            //   visible={this.state.visible}
            //   onCancel={() => this.setState({visible:false})}
            //   onCreate={() => this.handleCreate}

            element={this.props.element}
            wrappedComponentRef={this.saveFormRef}
            visible={this.state.visible}
            onCancel={this.handleCancelled}
            onCreate={this.handleCreate}
            confirmLoading={this.state.confirmLoading}
          />

        </Row>
        <Table
          dataSource={data} columns={this.state.columns} bordered className="table-striped-rows"
          pagination={{ defaultPageSize: 10, showSizeChanger: true, pageSizeOptions: ['5', '10', '20', '30'] }}
          rowKey="SerialNumber" />
      </div>
    )
  };
};

const mapStateToProps = (state) => ({
  counterparties: state.fundtransfer.counterparties,
  accountSummary: state.loginData.accountSummaryDetails.accounts,
  balance: state.loginData.accountSummaryDetails.overall_balance,
  globalMessage: state.globalMessage,
  token: state.loginData.token,
});

const ManageBeneficiaryForm = Form.create({ name: "ManageBeneficiary" })(
  ManageBeneficiary
);

export default connect(mapStateToProps, {
  CounterPartiesCreation,
  setGlobalMessage,
  clearGlobalMessage,
  FetchAccountSummary,
  FetchCounterParties
})(ManageBeneficiaryForm);
