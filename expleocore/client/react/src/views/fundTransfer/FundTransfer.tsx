import React, { Component } from "react";
import { connect } from "react-redux";
import { Container } from "shards-react";
import Popup from "reactjs-popup";
import Global from "../../components/layout/Global/Global";
import { Row, Input, Form, Button, Layout, Spin, Alert, Select, Card, Col, InputNumber } from "antd";
import { transactionRequest, FetchCounterParties } from '../../redux/actions/FundTransfer/fundTransferAction';
import { setGlobalMessage, clearGlobalMessage } from "../../redux/actions/messageHandlerAction";
import { logoutAction, FetchAccountSummary } from '../../redux/actions/login/loginAction';
import Error from "../../assets/images/avatars/Error.svg";
import Success from "../../assets/images/avatars/tick-icon.svg"
import "antd/dist/antd.css";
import { SSL_OP_NO_SESSION_RESUMPTION_ON_RENEGOTIATION } from "constants";


const { Option } = Select;
const { TextArea } = Input;
let filterValue;

type MyProps = {
  form: any,
  counterparties: any,
  accountSummary: any,
  balance: any,
  account: any,
  token: any,
  transactionRequest: any,
  FetchCounterParties: any,
  accounts: any,
  globalMessage: any;
  clearGlobalMessage: any;
  FetchAccountSummary: any;
  loading: any;
};

type MyState = {
  popState: boolean;
  loading: boolean;

};

class FundTransfer extends Component<MyProps, MyState> {
  //  componentWillMount() {
  //   if(this.props.loading===false && this.state.loading){
  //     Global.history.push("/home");
  //   }
  //   }


  constructor(props: any) {
    super(props);
    this.state = {
      popState: true,
      loading: false
    };
  }

  handleSubmit = (e: any) => {
    e.preventDefault();
    (document.getElementById('Demo') as HTMLInputElement).disabled = true;
    this.props.form.validateFieldsAndScroll(async (err: any, values: any) => {
      if (!err) {
        this.setState({ loading: true });
        if (this.props.token !== null && this.props.token !== undefined && this.props.accountSummary[0].id !== undefined) {
          let loginData = {
            token: this.props.token,
            accountId: this.props.accountSummary[0].id
          }
          console.log("transfer---->", loginData, values);
          let response1 = await this.props.transactionRequest(values, loginData);
          console.log("response1", response1);
          if (response1 !== "error" && response1.status === 201) {
            let response2 = await this.props.FetchAccountSummary(this.props.token);
            console.log("response2", response2);
          }
        }

      }
    });
  };

  // FetchAccountSummary(){
  //   if (this.props.token) {
  //     this.props.FetchAccountSummary(this.props.token);
  //     }
  //       Global.history.push("/home");
  // }

  onSearch(input, option) {
    return option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
  }

  // clearGlobalmessage = () => {
  //   this.props.clearGlobalMessage();
  // };

  // handleClose() {
  //   Global.history.push("/home");
  // }

  closeModal = () => {
    this.props.clearGlobalMessage();
    this.setState({ popState: false, loading: false });
    Global.history.push("/home");
    // if(this.props.globalMessage.status){
    //   this.FetchAccountSummary();
    // }
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    return (
      <Form>
        <fieldset id="Demo">
          <div className="header">
            Fund Transfer
        </div>
          <div className="container-fluid">
            <Row type="flex" justify="center">
              <Col span={8}>
                {this.props.globalMessage.status ? (
                  this.props.globalMessage.status === "SUCCESS" ||
                    this.props.globalMessage.status === "success" ? (
                      <Popup
                        open={this.state.popState}
                        closeOnDocumentClick
                        onClose={this.closeModal}
                      >
                        <div className="pop-up">
                          <a className="close" onClick={this.closeModal}>
                            &times;
                      </a>
                          <img className="popup-icon" src={Success} alt="Success" />
                          <h5 className="pop-text">{this.props.globalMessage.message}</h5>
                        </div>
                      </Popup>
                    ) : (
                      <Popup
                        open={this.state.popState}
                        closeOnDocumentClick
                        onClose={this.closeModal}
                      >
                        <div className="pop-up">
                          <a className="close" onClick={this.closeModal}>
                            &times;
                      </a>
                          <img className="popup-icon" src={Error} alt="Error" />
                          <h5 className="pop-text">{this.props.globalMessage.message}</h5>
                        </div>
                      </Popup>
                    )
                ) : null}
              </Col>
            </Row>
            <div className="cardheader">
              <Card className="antcardborder">
                <div className="row">
                  <div className="col-md-6 col-sm-12">
                    <Form.Item>
                      {getFieldDecorator("beneficiary_name", {
                        rules: [
                          {
                            message: "Please Select Beneficiary",
                            required: true,
                          }
                        ]
                      })(
                        <Select
                          showSearch
                          placeholder="Select Beneficiary"
                          filterOption={(input, option) => this.onSearch(input, option)
                          }
                        >
                          {this.props.counterparties.map(obj => (
                            <Option key={obj.name} value={obj.other_account_routing_address}>
                              {obj.name}
                            </Option>
                          ))}

                        </Select>
                      )}
                    </Form.Item>
                  </div>


                  <div className="col-md-6 col-sm-12">
                    <Form.Item>
                      {getFieldDecorator("amount", {
                        rules: [
                          {
                            required: true,
                            message: "Please enter amount",
                          },
                          // {
                          //   max: 100,
                          //   message:
                          //     "amount should be accept maximum 100 characters"
                          // },
                          {
                            type: 'number',
                            min: 0,
                            message: "Please Enter Valid Amount",
                          }
                        ]
                      })(<InputNumber placeholder="Amount" />)}
                    </Form.Item>
                  </div>

                  <div className="col-md-6 col-sm-12">
                    <Form.Item>
                      {getFieldDecorator("description", {
                        rules: [
                          {
                            message: "Please Enter Description",
                            required: true,
                          },
                          {
                            max: 1000,
                            message:
                              "description should be accept maximum 1000 characters"
                          },
                          {
                            message: "description should be minimum 2 characters",
                            min: 2
                          }
                        ]
                      })(<Input placeholder="Description" />)}
                    </Form.Item>
                  </div>
                  <div className="col-md-12 btn-align">
                    <Button
                      type="primary"
                      onClick={this.handleSubmit} loading={this.state.loading}
                    >
                      {/* <Spin spinning={this.state.loading} delay={500}></Spin> */}
                    Transfer
                </Button>
                  </div>
                </div>
              </Card>
            </div>
          </div>
        </fieldset>
      </Form>
    );
  }
}

const mapStateToProps = (state: any) => ({
  counterparties: state.fundtransfer.counterparties,
  accountSummary: state.loginData.accountSummaryDetails.accounts,
  token: state.loginData.token,
  accounts: state.loginData.accountSummaryDetails.accounts,
  globalMessage: state.globalMessage,
  loading: state.loginData.loading
});

const FundTransferForm = Form.create({ name: "FundTransfer" })(FundTransfer);

export default connect(mapStateToProps, {
  transactionRequest,
  FetchCounterParties,
  FetchAccountSummary,
  clearGlobalMessage
})(FundTransferForm);

