import { FETCH_COUNTERPARTIES,MONEY_TRANSFER } from '../../actions/types';

const initialState = {
  counterparties:[],
  moneytransfer:{}
}

export default function (state = initialState, action: any) {
    switch (action.type) {
        case FETCH_COUNTERPARTIES:
            return {
                ...state,
                counterparties: action.payload
            };
            case MONEY_TRANSFER:
            return {
                ...state,
                moneytransfer: action.payload
            };
        default:
            return state;
    }
}
