import { LOGIN, LOGOUT, ACCOUNT_SUMMARY, LOGIN_DETAILS ,LOADING} from '../../actions/types';

const initialState = {
    Email: null,
    isLoggedIn: false,
    redirect: false,
    token: null,
    accountSummaryDetails: {},
    loginDetails: [],
    loading: true
}

export default function (state = initialState, action: any) {
    switch (action.type) {
        case LOGIN:
            return {
                ...state,
                ...action.payload
            };
        case LOGOUT:
            return {
                ...state,
                ...action.payload
            };
        case ACCOUNT_SUMMARY:
            return {
                ...state,
                accountSummaryDetails: action.payload
            };
        case LOGIN_DETAILS:
            return {
                ...state,
                loginDetails: action.payload
            };
        case LOADING:
            return {
                ...state,
                ... action.payload
            };
        default:
            return state;
    }
}
