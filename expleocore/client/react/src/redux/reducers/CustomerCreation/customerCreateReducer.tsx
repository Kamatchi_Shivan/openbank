import { CREATE_USER ,CREATE_ACCOUNT  ,CUSTOMER_CREATE_LOGIN} from '../../actions/types';

const initialState = {
  createUser:[],
  createAccount:[],
  usertoken:{}
}

export default function (state = initialState, action: any) {
    switch (action.type) {
        case CREATE_USER:
            return {
                ...state,
                createUser: action.payload
            };
            case CREATE_ACCOUNT:
                return {
                    ...state,
                    createAccount: action.payload
                };
                    case CUSTOMER_CREATE_LOGIN:
                        return {
                            ...state,
                            usertoken:action.payload
                        };
        default:
            return state;
    }
}
