import {combineReducers} from 'redux';
import loginReducer from '../reducers/login/loginReducer';
import languageReducer from "../reducers/language/languageReducer";
import Beneficiary from "../reducers/AddBeneficiary/AddBeneficiary";
import fundtransfer from "../reducers/fundTransfer/fundtransferReducer";
import customerCreation from "../reducers/CustomerCreation/customerCreateReducer";
import messageHandlerReducer from "./messageHandlerReducer";

const appReducer = combineReducers({
    languageDatum : languageReducer,
    loginData: loginReducer,
    Beneficiary: Beneficiary,
    fundtransfer: fundtransfer,
    customerCreation: customerCreation,
    globalMessage: messageHandlerReducer,    
});
const rootReducer = (state, action) => {
    if (action.type === 'LOGOUT') {
      state = {}
    }
  
    return appReducer(state, action)
  }
  
  export default rootReducer;
