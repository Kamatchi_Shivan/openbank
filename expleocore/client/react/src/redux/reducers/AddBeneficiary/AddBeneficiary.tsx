import { ADD_BENEFICIARY } from '../../actions/types';

const initialState = {
  addBeneficiary:{}
}

export default function (state = initialState, action: any) {
    switch (action.type) {
        case ADD_BENEFICIARY:
            return {
                ...state,
                addBeneficiary: action.payload
            };
        default:
            return state;
    }
}
