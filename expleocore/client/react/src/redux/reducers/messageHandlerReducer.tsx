import {MESSAGE_HANDLER, SET_MESSAGE} from "../actions/types";

const initialState = {};

export default function(state = initialState, action) {
  switch (action.type) {
    case MESSAGE_HANDLER:
      return {
        ...action.payload
      };
    case SET_MESSAGE:
      return action.payload;
    default:
      return state;
  }
}
