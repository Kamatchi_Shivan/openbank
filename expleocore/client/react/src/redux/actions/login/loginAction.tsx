import { LOGIN, LOGOUT, ACCOUNT_SUMMARY ,LOGIN_DETAILS ,LOADING ,CUSTOMER_CREATE_LOGIN } from '../types';
import fetch from '../../../utils/leoFetch'
import axios from '../../../utils/leoAxios'
import history from '../../../history';
// import Global from '../../../../src/components/layout/Global/Global.js';
import Global from '../../../components/layout/Global/Global';
import message from '../../../components/messages/message.json'
import { config } from '../../../config/configs'

let cryptoJs = require('crypto-js');
let jwtDecode = require('jwt-decode');

export const logoutAction = () => (dispatch: any) => {
  let loginData = {
    isLoggedIn: false,
    redirect: false,
    token: null
  };
  //localStorage.clear();
  

  dispatch({
    payload: loginData,
    type: LOGOUT
  })
  Global.history.push("/login");

}

export const loginAction = (values: any,userName:any) => (dispatch: any) => {
  console.log("loginAction called",values,userName)
  let payload = {
    userName: values.userName,
    password: values.password
  }
  let loginData = {
    Name: null,
    userName: null,
   // isLoggedIn: false,
    password: null, // for reference
   // redirect: false,
    token: null
  };

  let auth = "DirectLogin username=" + values.userName + ",password=" + values.password + ",consumer_key=0hxgkb5caccko4i0uwitaj2ataxvzsvpbvgfcpef";
  return new Promise(function (resolve, reject) {

  axios.post(config.apiRootPath + config.authenticationURL, payload, {
    headers: {
      'Content-Type': 'application/json',
      'Authorization': auth,
    }
  })
    .then(res => {
      let token = res.data.token;
      if(userName===config.adminUser){
          dispatch({
            payload: {
             ...res.data
            },
            type: CUSTOMER_CREATE_LOGIN
        })
      }else{
        if (token !== null && token !== undefined) {
          let decoded = jwtDecode(token);
          dispatch({
            payload: {
             // token: decoded.token,
              //status: decoded.status,
              //isLoggedIn: true,
             // redirect: true,
              ...res.data
             // message: message.loginSuccessMessage,
            },
            type: LOGIN
          });
        
        } else {
          //  alert("Username and Password does not match");
          // localStorage.removeItem("token");
          // Global.alertContent = {
          //     closable: true,
          //     message: "Login Failed",
          //     status: "error"
          // }
          dispatch({
            payload: {
              ...loginData,
              message: message.loginErrorMessage
            },
            type: LOGIN
          })
        }
      }
     return resolve(res);
    })
    .catch(error => {
      console.log("error",error)
       //  alert("Error: Request failed with status code 401");
        //localStorage.removeItem("token");
    //     Global.alertContent = {
    //         closable: true,
    //         message: error,
    //         status: "error"
    //     }
    if(userName===config.adminUser){
      dispatch({
        payload: {
            ...loginData,
            message: message.loginErrorMessage
        },
        type: CUSTOMER_CREATE_LOGIN
    })
    }else{
      dispatch({
        payload: {
            ...loginData,
            message: message.loginErrorMessage
        },
        type: LOGIN
    })
    }
    return resolve("error"); 
     });
    });
    
}

// export const changePassword = (values: any) => async (dispatch: any) => {
//   console.log(123, values.Email);
//   var options = {
//     ConfirmPassword: values.ConfirmPassword,
//     Email: values.Email,
//     OldPassword: values.OldPassword
//   };
//   await axios.put(config.apiRootPath + config.changePasswordURL, options).then(resp => {
//     if (resp.data.status === "SUCCESS") {
//       Global.alertContent = {
//         closable: false,
//         message: resp.data.userMessage,
//         status: "success"
//       };
//       dispatch({
//         payload: {},
//         type: CHANGE_PASSWORD
//       });
//     } else {
//       Global.alertContent = {
//         closable: true,
//         message: resp.data.userMessage,
//         status: "error"
//       };
//       dispatch({
//         payload: {},
//         type: CHANGE_PASSWORD
//       });
//     }
//   });
// };

// export const getMetaData = (values: any) => async (dispatch: any) => {

//   var options = {
//     hostname: window.location.hostname
//   };
//   console.log("To backend : ", options, values);
//   await axios.post(config.apiRootPath + config.getMetaDataURL, options).then(resp => {
//     var bytes = cryptoJs.AES.decrypt(resp.data['metaData'], config.secretKey);
//     let data = bytes.toString(cryptoJs.enc.Utf8);
//     console.log(102, data);
//     let decryptedData = JSON.parse(data);
//     decryptedData.style = JSON.parse(decryptedData.style);
//     decryptedData.other_metadata = JSON.parse(decryptedData.other_metadata);
//     dispatch({
//       payload: decryptedData,
//       type: META_DATA
//     });
//   });
// };


export const FetchAccountSummary = (token: any ) => (dispatch: any) => {
  let loginData = {
    Name: null,
    userName: null,
    password: null,
    token: null
  };
  let config1 = {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      'Authorization': 'DirectLogin token=' + token
    }
  };

  return new Promise(function (resolve, reject) {
  axios.get('https://cors-anywhere.herokuapp.com/https://apisandbox.openbankproject.com/obp/v4.0.0/banks/'+config.BANK_ID+'/balances', config1)
    .then(res => {
      console.log("In Res Dispatch", res)
      dispatch({
        payload: {
          ...res.data
        },
        type: ACCOUNT_SUMMARY
      })
      dispatch({
        payload: {
          loading:false
        },
        type: LOADING
      })
      dispatch({
        payload: {
          token: token,
          isLoggedIn: true,
          redirect: true,
          message: message.loginSuccessMessage
        },
        type: LOGIN
      });
    return resolve(res);
    })
    .catch(error => {
      console.log("Error---->",error)
      dispatch({
        payload: {
          ...loginData,
          role:"customer",
          message: message.loginErrorMessage
        },
        type: LOGIN
      })
    return resolve("error");
  });
});
};

export const inmemoryloginDetails = (values: any) => async (dispatch: any) => {
  return new Promise(function (resolve, reject) { 
  dispatch({
    payload: values,
    type: LOGIN_DETAILS
  });
  return resolve("success")
});
};
