import { ADD_BENEFICIARY,MESSAGE_HANDLER,LOADING } from '../types';
import axios from '../../../utils/leoAxios';
import message from '../../../components/messages/message.json';
import { config } from '../../../config/configs'


export const CounterPartiesCreation = (Values: any,data:any) => (dispatch: any) => {
//data.token = "eyJhbGciOiJIUzI1NiJ9.eyIiOiIifQ.LkCLXxrhbzLoqzcVowYz5MwVt6IRvt5WuPeClNbMNHQ";
Values = {
  name: Values.name,
  description:  Values.description,
  other_account_routing_scheme: "OBP",
  other_account_routing_address:Values.accountNumber ,
  other_account_secondary_routing_scheme: "IBAN",
  other_account_secondary_routing_address: "DE89370400440532013000",
  other_bank_routing_scheme: "OBP",
  other_bank_routing_address: config.BANK_ID,
 // other_bank_routing_address: "in-bank-x-",
  other_branch_routing_scheme: "OBP",
  other_branch_routing_address: "12f8a9e6-c2b1-407a-8bd0-421b7119307e",
  is_beneficiary: true,
  bespoke: [
      {
          key: "englishName",
          value: "english Name"
      }
  ]
};
return new Promise(function (resolve, reject) {

  axios.post('https://cors-anywhere.herokuapp.com/https://apisandbox.openbankproject.com/obp/v4.0.0/banks/'+config.BANK_ID+'/accounts/'+data.accountId+'/owner/counterparties', Values, {
    headers: {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json',
      'Authorization': 'DirectLogin token=' + data.token
    }
  })
    .then(res => {
      if(res.status ===  201){
        dispatch({
          payload: {
            ...res.data
          },
          type: ADD_BENEFICIARY,
        });
        dispatch({
          payload: {
            closable: false,
            message: message.beneficiarySuccessMessage,
            status: "success"
          },
          type: MESSAGE_HANDLER
        });
        dispatch({
          payload: {
            loading:true
          },
          type: LOADING,
        });

      }else{
        dispatch({
          payload: {
            closable: true,
            message:message.beneficiaryErrorMessage,
            status: "error"
          },
          type: MESSAGE_HANDLER
        });
      }
      return resolve(res);
    })
    .catch(error => {
      console.log("Error---->",error)
      dispatch({
        payload: {
          closable: true,
          message:message.beneficiaryErrorMessage,
          status: "error"
        },
        type: MESSAGE_HANDLER
      });
      return resolve("error");
  });
});
};
