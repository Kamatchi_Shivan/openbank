import { FETCH_COUNTERPARTIES, MONEY_TRANSFER, LOADING, MESSAGE_HANDLER} from '../types';
import fetch from '../../../utils/leoFetch'
import axios from '../../../utils/leoAxios'
import history from '../../../history';
// import Global from '../../../../src/components/layout/Global/Global.js';
import Global from '../../../components/layout/Global/Global';
import message from '../../../components/messages/message.json'
import { config } from '../../../config/configs'

 export const FetchCounterParties = (data: any ) => (dispatch: any) => {
   console.log("FetchCounterParties Action Calling...")
  return new Promise(function (resolve, reject) {
   
   axios.get('https://cors-anywhere.herokuapp.com/https://apisandbox.openbankproject.com/obp/v4.0.0/banks/'+config.BANK_ID+'/accounts/'+data.accountId+'/owner/counterparties',
    {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
          'Authorization': 'DirectLogin token=' + data.token
        }
      }
    )
      .then(res => {
        dispatch({
          payload: res.data.counterparties,
          type: FETCH_COUNTERPARTIES
        });
        return resolve(res);
      })  
      .catch(error => {
        return resolve("error");
    });
  });
  };

  export const transactionRequest = (values: any,data:any) => (dispatch: any) => {
  console.log("TransactionRequest Action",values,data);
    let Values = {
      to:{
        bank_id:config.BANK_ID,
        account_id: values.beneficiary_name
      },
      value:{
        currency:"EUR",
        amount: values.amount
      },
      description: values.description
    };
   
    return new Promise(function (resolve, reject) { 
      axios.post('https://cors-anywhere.herokuapp.com/https://apisandbox.openbankproject.com/obp/v4.0.0/banks/'+config.BANK_ID+'/accounts/'+data.accountId+'/owner/transaction-request-types/SANDBOX_TAN/transaction-requests', Values, {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
          'Authorization': 'DirectLogin token=' + data.token
        }
      })
        .then(res => {
          if(res.status ===  201){
            dispatch({
              payload: {
                ...res.data
              },
              type: MONEY_TRANSFER,
            });
            dispatch({
              payload: {
                loading:true
              },
              type: LOADING,
            });
            dispatch({
              payload: {
                closable: false,
                message: message.FundSucessMessage,
                status: "success"
              },
              type: MESSAGE_HANDLER
            });
    
          }else{
            dispatch({
              payload: {
                closable: true,
                message:message.FundErrorMessage,
                status: "error"
              },
              type: MESSAGE_HANDLER
            });
          }
          return resolve(res);
        })
        .catch(error => {
          console.log("Error---->",error)
          dispatch({
            payload: {
              closable: true,
              message:message.FundErrorMessage,
              status: "error"
            },
            type: MESSAGE_HANDLER
          });
          return resolve("error");
      });
    });
    };
    
  