import { SET_MESSAGE , MESSAGE_HANDLER} from "./types";
import fetch from "../../utils/leoFetch";
import axios from "../../utils/leoAxios";
import Global from "../../../src/components/layout/Global/Global";

export const setGlobalMessage = message => async dispatch => {
  dispatch({
    payload: message,
    type: SET_MESSAGE
  });
};

export const clearGlobalMessage = () => dispatch => {
  dispatch({
    payload: [],
    type: SET_MESSAGE
  });
  dispatch({
    payload: [],
    type: MESSAGE_HANDLER
  });
};
