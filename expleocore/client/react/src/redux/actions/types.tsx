export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";
export const FETCH_POSTS = "FETCH_POSTS";
export const GET_CONVERSION_RATE = "GET_CONVERSION_RATE";
export const GET_USD_RATE = "GET_USD_RATE";
export const TOGGLE_PANEL = "TOGGLE_PANEL";
export const GET_EUR_RATE = "GET_EUR_RATE";
export const GET_MODEL = "GET_MODEL";
export const VIN_VALIDATE = "VIN_VALIDATE";
export const SAVE_CONTRACT = "SAVE_CONTRACT";
export const DEPLOY_CONTRACT = "DEPLOY_CONTRACT";
export const MESSAGE_HANDLER = "MESSAGE_HANDLER";
export const SET_MESSAGE = "SET_MESSAGE";
export const LANGUAGE = "LANGUAGE";
export const LOGIN_DETAILS = "LOGIN_DETAILS";

/*Account Summary*/
export const ACCOUNT_SUMMARY = "ACCOUNT_SUMMARY";
export const ADD_BENEFICIARY = "ADD_BENEFICIARY";
export const LOADING = "LOADING";

/* Fund Transfer*/
export const FETCH_COUNTERPARTIES = "FETCH_COUNTERPARTIES";
export const MONEY_TRANSFER = "MONEY_TRANSFER";

/* Customer Creation*/
export const CREATE_USER = "CREATE_USER";
export const CREATE_ACCOUNT = "CREATE_ACCOUNT";
export const CUSTOMER_CREATE_LOGIN = "CUSTOMER_CREATE_LOGIN";



