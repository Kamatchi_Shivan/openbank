import { CREATE_USER ,MESSAGE_HANDLER,CREATE_ACCOUNT } from '../types';
import axios from '../../../utils/leoAxios'
import Global from '../../../components/layout/Global/Global';
import message from '../../../components/messages/message.json'
import { config } from '../../../config/configs'


/*Create User*/

export const CreateUser = (values:any,token:any) => (dispatch: any) => {
  console.log("create Customer action")
    let body={
        email:values.email,
        username:values.userName,
        password:values.password,
        first_name:values.fname,
        last_name:values.lname
    }
   
    return new Promise(function (resolve, reject) {
        axios.post('https://cors-anywhere.herokuapp.com/https://apisandbox.openbankproject.com/obp/v4.0.0/users', body, {
          headers: {
              'Access-Control-Allow-Origin': '*',
              'Content-Type': 'application/json',
              'Authorization': '*',
          }
        })
          .then(res => {
            console.log("create User response",res);
            if(res.status ===  201){
              dispatch({
                payload: {
                  ...res.data,
                },
                type: CREATE_USER,
              });
              dispatch({
                payload: {
                  closable: false,
                  message: message.UserCreated,
                  status: "success"
                },
                type: MESSAGE_HANDLER
              });
               
          }else if(res.status ===  409){
              dispatch({
                  payload: {
                      closable: false,
                      message: message.DuplicateUser,
                      status: "Failed"
                  },
                  type: MESSAGE_HANDLER,
                });
          }
          return resolve(res);
          })
          .catch(error => {
            console.log("error",error);
            dispatch({
              payload: {
                  closable: false,
                  message: message.DuplicateUser,
                  status: "Failed"
              },
              type: MESSAGE_HANDLER,
            });
            return resolve("error");
           }
           )
          
          });
}

/*Account Creation*/

export const CreateAccount = (values:any ,user:any ,token:any) => (dispatch: any) => {
  console.log("create Account action",values,user,token);

    let body={
        user_id:user.user_id,
        label:values.userName,
        product_code:"AC",
        balance:{
            currency:"EUR", 
            amount:"0"
        },
        branch_id:"DERBY6",
        account_routing:{
            scheme:"AccountNumber",
            address:"4930396"
        }
    }
    let config1 = {
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json',
          'Authorization': 'DirectLogin token=' + token
        }
      };     
  return new Promise(function (resolve, reject) {
    axios.post('https://cors-anywhere.herokuapp.com/https://apisandbox.openbankproject.com/obp/v4.0.0/banks/'+config.BANK_ID+'/accounts', body, config1
      )
        .then(res => {
          console.log("create account response",res);
          if(res.status ===  201){
            dispatch({
              payload: {
                ...res.data
              },
              type: CREATE_ACCOUNT,
            });
            dispatch({
              payload: {
                closable: false,
                message: message.AccountSuccessMessage,
                status: "success"
              },
              type: MESSAGE_HANDLER
            });
              return resolve(res);
        }else{
            dispatch({
                payload: {
                  closable: false,
                  message: message.AccountErrorMessage,
                  status: "failed"
                },
                type: MESSAGE_HANDLER
              });
        }
        return resolve(res);
        })
        .catch(error => {
          dispatch({
            payload: {
                closable: false,
                message: message.NetworkError,
                status: "Failed"
            },
            type: MESSAGE_HANDLER,
          });
          return resolve("error");
         });
        });
}